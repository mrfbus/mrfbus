import sys

from urllib import parse, request


def sms_send(url,user,passwd,numbers,msg):

    for num in numbers:
        params = parse.urlencode({'username' : user, 'password' : passwd,
                                   'message' : msg, 'msisdn' : num,
                                   'allow_concat_text_sms' : '1' ,
                                   'concat_text_sms_max_parts' : '2'}).encode("utf-8")

        f = request.urlopen(url, params)
        s = f.read()
        f.close()
        print ("SMS gateway returned:"+s.decode())


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description=__doc__,
                                    formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('msg', metavar='MSG', type=str,
                   help='sms message')
    parser.add_argument('--verbose', dest='verbose', action="store_true",
                default=False, help='Enable verbose log messages')
    parser.add_argument('--num', dest='num', type=str,nargs='+',
                    default=None, help='sms number')
    parser.add_argument('--url', dest='url', type=str,
                    default=None, help='sms url')
    parser.add_argument('--user', dest='user', type=str,
                    default=None, help='sms user')
    parser.add_argument('--passwd', dest='passwd', type=str,
                    default=None, help='sms pass')
    

    args = parser.parse_args()
    #print args

    sms_send(args.url,args.user,args.passwd,  args.num, args.msg)
