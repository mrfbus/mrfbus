/******************************************************************************
*
* Copyright (c) 2012-16 Gnusys Ltd
* Copyright (c) 2012-2021 Steve Murphy and the owners of gnusys.com
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
******************************************************************************/

// MODBUS for RS-485 implementation using UART


#include "mrf_types.h"
#include "mrf_modbus.h"



uint8_t ModbusHandler::idx;
uint8_t ModbusHandler::addr;
uint8_t *ModbusHandler::resp_buff;
uint8_t ModbusHandler::dbg_buff[10];

uint32_t ModbusHandler::errors;
uint16_t ModbusHandler::cmd_errs;
volatile uint16_t ModbusHandler::drv_errs;
uint16_t ModbusHandler::resp_errs;
uint8_t ModbusHandler::resp_errored;
uint8_t ModbusHandler::resp_len;
uint8_t ModbusHandler::waiting_resp;
//const uint8_t ModbusHandler::param_len=0;
uint16_t ModbusHandler::crc16;
uint16_t ModbusHandler::rx_crc16;
uint16_t ModbusHandler::rx_crc_errs;
uint16_t ModbusHandler::no_resp_errs;
PROC_TAG ModbusHandler::ptag;
//const uint8_t ModbusHandler::func=0;
//int *ModbusHandler::hw_handler(ModbusHandler *mh);


ModbusHandler::ModbusHandler (void){
    errors       = 0;
    cmd_errs     = 0;
    resp_errs    = 0;
    rx_crc_errs  = 0;
    waiting_resp = 0;
    no_resp_errs = 0;
  }

ModbusHandler::~ModbusHandler (void){}

ModbusReadHoldingRegs::ModbusReadHoldingRegs(){}
ModbusReadHoldingRegs::~ModbusReadHoldingRegs(){}

ModbusReadDeviceID::ModbusReadDeviceID(){}
ModbusReadDeviceID::~ModbusReadDeviceID(){}


static ModbusReadHoldingRegs  read_holding_regs = ModbusReadHoldingRegs();

//const uint8_t ModbusReadHoldingRegs::func = 0x03;


//const uint8_t ModbusReadDeviceID::func=0;


// declare instances of statics 
uint8_t ModbusReadDeviceID::resp_obs;
uint8_t ModbusReadDeviceID::ob_num;
uint8_t ModbusReadDeviceID::ob_len;
uint8_t ModbusReadDeviceID::ob_idx;
uint8_t ModbusReadDeviceID::ob_st;
//const uint8_t ModbusReadHoldingRegs::func = 0x03;


const uint8_t ModbusReadDeviceID::modbus_read_devid_params[3] =  { 0x0e , 0x01, 0x00 };

/*
static ModbusReadDeviceID read_dev_id =  ModbusReadDeviceID();


uint8_t mrf_modbus_read_devid(uint8 addr, PROC_TAG ptag, uint8 *resp_buff, uint8 *resp_len ) {
  uint8_t err;
  err = read_dev_id.issue_cmd(addr,ptag, resp_buff);
  //read_dev_id.issue_cmd(addr,resp_buff);
  *resp_len = read_dev_id.resp_len;
  return err;
}
*/
volatile int rx_crc_errors = 0;
void modbus_rx_crc_error(){ // something for gdb to break on
  rx_crc_errors++;
}


/* modbus , can pass in param to generic function for requests, length is known
            response length can only be determined at run time in the general case, based on the function code
            each supported function code must have a response parse/length predictor.
            these predictors , in the general case would need some internal state
            can't use derived classes currently, 
*/



extern "C" void __cxa_pure_virtual() 
{ 
    while (true) {} 
}

