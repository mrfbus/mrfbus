/******************************************************************************
*
* Copyright (c) 2012-16 Gnusys Ltd
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
******************************************************************************/

#include "mrf_sys.h"
#include "mrf_kdata.h"
const uint8 _mrfid = MRFID;

#define _DEVNAME_STR_  SYM_NAME(_CONCAT_(DEVTYPE,MRFID))

const MRF_PKT_DEVICE_INFO device_info  = { SYM_NAME(DEVTYPE) ,SYM_NAME(ARCH),   MRFID, MRFNET, _MRF_BUFFS,NUM_INTERFACES , MRF_CLK_MHZ, MRF_CHANN_OFFSET};
const MRF_PKT_SYS_INFO sys_info        = {  SYM_NAME(GITSH), SYM_NAME(MRFBLD), /*(const uint8)*/MRF_NUM_SYS_CMDS,GITMOD };
const MRF_PKT_APP_INFO app_info        = {SYM_NAME(MRF_APP), MRF_NUM_APP_CMDS};


extern const MRF_CMD mrf_sys_cmds[MRF_NUM_SYS_CMDS] = {
  {"ACK"          , MRF_CFLG_NO_ACK               , 0                          , 0                            ,  NULL               , mrf_task_ack      },
  {"RETRY"        , MRF_CFLG_NO_ACK               , 0                          , 0                            ,  NULL               , mrf_task_retry      },
  {"RESP"         , MRF_CFLG_NO_ACK               , sizeof(MRF_PKT_RESP)       , 0                            ,  NULL               , mrf_task_resp     },
  {"DEVICE_INFO"  , 0                             , 0                          , sizeof(MRF_PKT_DEVICE_INFO)  ,  (void*)&device_info, NULL },
  {"DEVICE_STATUS", 0                             , 0                          , sizeof(MRF_PKT_DEVICE_STATUS),  NULL               , mrf_task_device_status },
  {"SYS_INFO"     , 0                             , 0                          , sizeof(MRF_PKT_SYS_INFO)     ,  (void*)&sys_info   , NULL },
  {"IF_STATUS"    , 0                             , sizeof(MRF_PKT_UINT8)      , sizeof(IF_STATS      )       ,  NULL               , mrf_task_if_status  },
  {"GET_TIME"     , 0                             , 0                          , sizeof(MRF_PKT_TIMEDATE)     ,  NULL               , mrf_task_get_time },
  {"SET_TIME"     , 0                             , sizeof(MRF_PKT_TIMEDATE)   , sizeof(MRF_PKT_TIMEDATE)     ,  NULL               , mrf_task_set_time   },
  {"BUFF_STATE"   , 0                             , sizeof(MRF_PKT_UINT8)      , sizeof(MRF_PKT_BUFF_STATE)   ,  NULL               , mrf_task_buff_state }  ,
  {"CMD_INFO"     , 0                             , sizeof(MRF_PKT_UINT8)      , sizeof(MRF_PKT_CMD_INFO)     ,  NULL               , mrf_task_cmd_info   },
  {"APP_INFO"     , 0                             , 0                          , sizeof(MRF_PKT_APP_INFO)     ,   (void*)&app_info  , NULL},
  {"APP_CMD_INFO" , 0                             , sizeof(MRF_PKT_UINT8)      , sizeof(MRF_PKT_CMD_INFO)     ,  NULL               , mrf_task_app_cmd_info   },
  {"TEST_1"       , 0                             , 0                          , sizeof(MRF_PKT_TIMEDATE)     ,  NULL               , mrf_task_test_1   },
  {"USR_STRUCT"   , 0                             , sizeof(MRF_PKT_RESP)       , 0                            ,  NULL               , mrf_task_usr_struct  },
  {"USR_RESP"     , 0                             , sizeof(MRF_PKT_RESP)       , 0                            ,  NULL               , mrf_task_usr_resp     },
  {"RESET"        , MRF_CFLG_NO_ACK               , 0                          , 0                            ,  NULL               , mrf_task_reset      },
  {"PING"         , 0                             , 0                          , sizeof(MRF_PKT_PING_RES)     ,  NULL               , mrf_task_ping},
  {"NDR"          , 0                             , sizeof(MRF_PKT_NDR)        , 0                            ,  NULL               , mrf_task_ndr      }

};



const uint16 mrf_num_cmds = (uint16)MRF_NUM_SYS_CMDS;  // FIXME -better to have user commands separate from sys
/*
const MRF_CMD *mrf_cmd_ptr(uint8 type){
  if (type >= MRF_NUM_SYS_CMDS)
    return NULL;
  return &mrf_sys_cmds[type];
}
*/
