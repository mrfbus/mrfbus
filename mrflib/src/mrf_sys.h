/******************************************************************************
*
* Copyright (c) 2012-16 Gnusys Ltd
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
******************************************************************************/

#ifndef __MRF_SYS_INCLUDED__
#define __MRF_SYS_INCLUDED__

#define _QUOTE_SYM_(SYM)  #SYM
#define SYM_NAME(name) _QUOTE_SYM_(name)

#define _CONCAT_(A,B) # A ## B

#include "device.h"
#include "mrf_types.h"
#include "mrf_sys_structs.h"
#include "mrf_if.h"
#include "mrf_buff.h"
#include "mrf_sys_cmds.h"
#include "mrf_route.h"

#define FALSE 0
#define TRUE  1

#define _MRF_BUFFLEN 64 // was 80 ideally, but limited with RF ATM FIME

// MRF CMD encoding regions


// 0 -63 SYS commands
#define _MRF_SYS_CMD_BASE 0   // app commands start at 128
#define _MRF_SYS_MAX_CMDS 64

// 64 - 127 SYS_QCMDS
#define _MRF_SYS_QCMD_BASE  (_MRF_SYS_CMD_BASE+_MRF_SYS_MAX_CMDS)  // app commands start at 128




// 128 - 191 APP CMDS 

#define _MRF_APP_CMD_BASE 128   // app commands start at 128
#define _MRF_APP_MAX_CMDS 64   // app commands start at 128

// 191 - 255 APP_QCMDS
#define _MRF_APP_QCMD_BASE  (_MRF_APP_CMD_BASE+_MRF_APP_MAX_CMDS)  // app commands start at 128
#define _MRF_APP_MAX_QCMDS 64
// 128-255 QEXT CMDS




typedef int (*MRF_APP_CALLBACK)(int fd);
//typedef void (*MRF_APP_MS_TIMEOUT)();

#ifndef HOSTBUILD
#define MRF_CMD_FUNC_DEC(dec)   dec
#else
#define MRF_CMD_FUNC_DEC(dec)   NULL
#endif


/* application signals are generated using reserved set of buffer numbers
   starting from MRF_BNUM_SIGNAL_BASE at the top of the bnum 8 bit space
*/
// OLD IDEA 
#define MRF_NUM_SIGNALS  32
#define MRF_BNUM_SIGNAL_BASE (256 - MRF_NUM_SIGNALS)

// NEW IDEA

// sysq command codes
#define SYSQ_CMD_EXE 0     // execute buffer according to the header type coding - includes app commands
#define SYSQ_CMD_NULL 0xff  // NULL code


#define IS_NULL_PTAG(ptag) (ptag.cmd == SYSQ_CMD_NULL) ? 1 : 0
#define SET_NULL_PTAG(ptag) ptag.cmd = SYSQ_CMD_NULL


// PROC_TAG cmd encoding is split with 128 values for system (0-127) and 128 for app (128-255)
//#define APP_CMD_BASE 0x80 // anything above this gets passed to app

// LEGACY DEFINES FOR APP_SIG - can keep for now
#define APP_SIG_SECOND  0
#define APP_SIG_TICK  1
#define APP_SIG_BUT1  2
#define APP_SIG_BUT2  3

#define APP_SIG_MS_TIMEOUT  16

#define APP_PTAG_MODBUS_BASE 0x40


/* constant flags */
#define MRF_CFLG_NO_ACK 1   // send no ack when segment recipient
#define MRF_CFLG_NO_RESP 2   // send no resp when final recipient
//#define MRF_CFLG_INTR 4  // RETIRING THIS ONE - task is run in interrupt handler

/*
typedef struct {
  const uint8 str[16];
  const uint8 cflags;
  const uint8 req_size;
  const uint8 rsp_size;
  const void *data;
  const MRF_CMD_FUNC func;
} MRF_CMD;
*/

int _mrf_process_packet(I_F owner,uint8 bnum);
//void _mrf_print_packet_header(MRF_PKT_HDR *hdr,I_F i_f);
void _mrf_tick();
void _mrf_print_hex_buff(uint8 *buff,uint16 len);
void mrf_print_packet_header(MRF_PKT_HDR *hdr);
void mrf_print_packet_header_dbg(uint8 debug_lvl,MRF_PKT_HDR *hdr);
uint8 *mrf_response_buffer(uint8 bnum);
int mrf_send_response(uint8 bnum,uint8 rlen);
int mrf_send_structure(uint8 dest, uint8 code,  uint8 *data, uint8 len);
int mrf_send_command_root(uint8 dest, uint8 code,  uint8 *data, uint8 len);
int mrf_send_command(uint8 dest, uint8 code,  uint8 *data, uint8 len);

uint16 mrf_copy(void *src,void *dst, size_t nbytes);
uint16 mrf_scopy(void *src,void *dst, size_t nbytes);
const MRF_CMD *mrf_cmd_ptr(uint8 type);
const MRF_CMD *mrf_app_cmd_ptr(uint8 type);
// these must be defined by every application
int mrf_app_init();
//int signal_handler(uint8 signal);

MRF_CMD_RES app_queue_process(PROC_TAG ptag);

int mrf_sys_queue_available();

// these are defined in arch  but here is prototype

// can be used by app
int mrf_wake();
int mrf_sleep();
int mrf_wake_on_exit();
int mrf_app_tick_enable(int secs);  // generate APP_SIG_TICK at interval secs
int mrf_app_tick_disable();
int mrf_app_ms_timeout(uint8 msecs, MRF_APP_CALLBACK cb);  // generate APP_MS_TIMEOUT 

// system functions defined in arch
int mrf_arch_boot();
int mrf_arch_run();
int mrf_tick_enable();  // this is actually system tick, not for app use
int mrf_tick_disable();
// end arch
void mrf_sys_init();
//int _mrf_buff_forward(uint8 bnum);
int _mrf_process_buff(uint8 bnum);
int mrf_foreground();
int mrf_sys_queue_push(PROC_TAG ptag);
int mrf_data_response(uint8 bnum,const uint8 *data,uint8 len);
int mrf_rtc_set(TIMEDATE *td);
int mrf_rtc_get(TIMEDATE *td);
int mrf_retry(I_F i_f,uint8 bnum);
int mrf_ndr(uint8 code, uint8 bnum);

// OLD style app signals TBD
int mrf_app_signal(uint8 signum, uint8 param1, uint8 param2);
// NEW style app PROC_TAG



#ifdef SLEEP_deep
int mrf_sleep_deep();  // must be defined by app for now
#endif

#include "mrf_sys_tasks.h"
//#include "mrf_sys_cmds.h"


#include "mrf_app.h"
#include "mrf_app_cmds.h"
#endif
