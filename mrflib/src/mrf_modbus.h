/******************************************************************************
*
* Copyright (c) 2012-16 Gnusys Ltd
* Copyright (c) 2012-2021 Steve Murphy and the owners of gnusys.com
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
******************************************************************************/

#ifndef _MRF_MODBUS_INCLUDED_
#define _MRF_MODBUS_INCLUDED_

//#include "mrf_types.h"
//#include "mrf_buff.h"
//#include "mrf_sys_structs.h"
#include "mrf_sys.h"
// MODBUS for RS-485 implementation using UART

// API designed for foreground (APP) usage 


// reserved app_queue codes required for implementation

#define APP_CMD_MODBUS_TURN_AROUND    32
#define APP_CMD_MODBUS_START_TX       33
#define APP_CMD_MODBUS_READ_DEV_ID    34
#define APP_CMD_MODBUS_READ_HOLD_REGS 35
#define APP_CMD_MODBUS_READ_METER_RDY 36

typedef enum {
  BAUD_9600  = 0,
  BAUD_19200 = 1,
  BAUD_38400 = 2
} MODBUS_BAUD;

typedef enum {
  PAR_ODD  = 0,
  PAR_EVEN = 1,
  PAR_NONE = 2
} MODBUS_PARITY;


int mrf_modbus_init(MODBUS_BAUD baud, MODBUS_PARITY parity);  // implemented in mrf_modbus_cc.c
int mrf_modbus_turn_around();  // implemented in mrf_modbus_cc.c
int mrf_modbus_start_tx();  // implemented in mrf_modbus_cc.c


// Only one Handler can be in use at a time , as only one modbus command/txn can run at a time
// Use static class variables to store data required by generic base class , to reduce RAM usage

// The we need a handler that understands the response of each command - i.e. to determine CRC pos and EOF in response byte stream
// So a derived class of ModbusHandler is required for each implemented command , which at a minimum overrides the rx_byte method

// bundle common tx logic here for convenience

typedef struct  {
  uint8 func_code;
  uint8 mei_type;
  uint8 req_code;
} ModbusReqObj;



// MRF_MODBUS_RXHANDLER checks response  rx_byte and signals end of pdu  
//typedef int (*MRF_MODBUS_RXHANDLER)(uint8_t rxb);


class ModbusHandler
{

 private:

 protected:
 public:
  static uint8_t addr, len,idx, resp_errored,waiting_resp;
  static uint8_t dbg_buff[10];
  static uint32_t errors;
  static uint16_t cmd_errs, resp_errs,rx_crc_errs, no_resp_errs;
  volatile static uint16_t drv_errs;
  static uint8  *resp_buff;
  //static  const uint8_t func;
  static uint16_t crc16, rx_crc16;
  static PROC_TAG ptag;
  static int hw_handler(ModbusHandler *mhandler);
  static uint8  resp_len;
  ModbusHandler();
  ~ModbusHandler();

  virtual uint8_t func()=0; //{return 0;}
  virtual uint16_t param(uint8_t idx)=0; //{return 0;}
  virtual uint8_t  param_len()=0;// {return 0;}
  virtual int     _rx_byte(uint8_t _rxb) = 0; //{return -1;};  // handles rx_byte until resp_len set

  //int  init_txn(uint8_t addr,    uint8 *resp_buff, uint8 resp_max_len);



  int start_rx(){
    // re-use idx for rx
    idx = 0;
    return 0;
  }
    
  uint16_t calc_crc16 (uint16_t crc, uint8_t data)
  {
    uint16_t LSB;
    crc = ((crc^(uint16_t)data) | 0xFF00) & (crc | 0x00FF);
    for (int i=0; i<8; i++) {
      LSB=(crc & 0x0001);
      crc=crc/2;
      if(LSB)
        crc=crc^0xA001;
    }
    return(crc);
  }

               
  int  init_txn(uint8_t addri,  PROC_TAG ptagi,  uint8 *resp_buffi){  // blocking task, should only be called by foreground application code
    int i;
    addr = addri  ; resp_buff = resp_buffi;
    idx = 0; resp_errored = 0;

    if (waiting_resp){ // no response from last
      if (ptag.bnum != 0xff)
        _mrf_buff_free(ptag.bnum); // free last buff, which was intended for a response , bnum must be initialised to 0xff to ignore
      no_resp_errs++;
    }
    ptag = ptagi;
    resp_len = 0;  // once resp_len is set then auto finishing by calling accept_rx_byte only

    // precalc crc
    crc16 = 0xffff;
    //crc_init();
    //crc_d8(addr);
    //crc_d8(func());
    crc16 = calc_crc16(crc16,addr);
    crc16 = calc_crc16(crc16,func());

    for (i = 0 ; i < this->param_len() ; i++)
      crc16 = calc_crc16(crc16,param(i));


    // call driver func / FIXME 
    waiting_resp = 1;
    hw_handler(this);  // this is actually just TX func - needs rename FIXME , should not block
    return 0 ; //mrf_modbus_send(this);
    
  }
  

  // see MODBUS over Serial Line specification  RTU mode for framing details implemented below 
  uint16_t tx_byte(){  // msb set when nothing left to tx - tx_byte is in low byte of uint16 if msb = 0
    uint16_t rv;
    if (idx == 0 ){
      rv = addr;
    }
    else if (idx == 1 ){
      rv = (uint16_t) func();
    }
    
    else if(idx < this->param_len() + 2){
      rv = (uint16_t) param(idx-2);
    }
    else if(idx == (this->param_len()+2)){
      rv = (uint16_t)(crc16 & 0xff);
    }
    else if(idx == (this->param_len()+3)){
      rv = (uint16_t)(crc16 >> 8);
    }
    else {
      rv =  0xffff;
    }

    if (rv != 0xffff && idx< 10) // tmp debug
      dbg_buff[idx] = rv;

    if (rv != 0xffff)
      idx++;

    return  rv;

  }

  uint8_t get_idx(){  // fixme half-hearted pointless use of getter for a public var - FIXME 
    return idx;
  }
    
  int rx_crc_error (){
    resp_errored = 1;
    rx_crc_errs++;
    return -1;
  }
  int accept_rx_byte(uint8_t _rxb){
    if (resp_errored)
      return -1;
    if (idx < sizeof(MRF_PKT_MAX_RESP)){
      resp_buff[idx++] = _rxb;
      return 0;
    }
    else{
      resp_errored = 1;
      return -1;
    }
  }
  int rx_byte(uint8_t _rxb){
    uint8_t err_code = 0;
   if (idx == 0 ) {
     if ( addr != _rxb) {  // all modbus responses return addr in first byte of resp
       resp_errored = 1;
       resp_errs++;
       //idx++;
       return -1;
     }

     return accept_rx_byte(_rxb);

   }
   else if (idx == 1 ) {
     if (func() != _rxb) {  // all modbus responses return func code in second byte of resp
       resp_errored = 1;
       resp_errs++;
       //idx++;
       return -1;
     }

     return accept_rx_byte(_rxb);

   }
   else {

     if (resp_errored)
       return -1;

     if(resp_len == 0){ // keep calling derived class until resp_len determined
       err_code = _rx_byte(_rxb);

       if(err_code == 0)
         return accept_rx_byte(_rxb);
       else return err_code;
     }
     else {
       if ( idx < resp_len ) // if resp_len has been set then auto accept
         return accept_rx_byte(_rxb);
       
       else if ( idx == resp_len ) {// first crc byte
         rx_crc16 = (uint16_t)_rxb;
         idx++;
       }
       else if ( idx == (resp_len + 1)){ // second crc byte
         rx_crc16 |= (uint16_t)(_rxb << 8);
         idx++;  // only for debug
         // FIXME should go async on CRC checks - can only work while single modbus channel uses CRC
         //crc_init();
         crc16 = 0xffff;

         for (int i = 0 ; i < resp_len ; i++) {
           crc16 = calc_crc16(crc16,resp_buff[i]);
           //crc_d8(resp_buff[i]);
         }
         //crc16 = crc_res();
         waiting_resp = 0;
         if (crc16==rx_crc16){
           ptag.param1 = idx; // set the response len in ptag.param1 
           mrf_sys_queue_push(ptag);
           return 1;  // packet received
         }
         else 
           return rx_crc_error();
          
       }
     }
     
   }
   return 0;
  }

  //virtual int issue_cmd(uint8_t addr, uint8_t *resp_buff);
};

// mrf_modbus_send implementation :  resp_buff is written by UART RX interrupt
// for APP (foreground usage only), not for system use
// normally called by higher level that knows expected response length

// resp_len is written with length of response
//int mrf_modbus_send(uint8 addr,uint8 len,  uint8 *buff, uint8 *resp_buff, uint8 resp_len,  MRF_APP_CALLBACK resp_cb);

//int mrf_modbus_send(ModbusHandler *mh);





// higher level API - supported operations are elaborated here


class ModbusReadHoldingRegs : public ModbusHandler {

 private : uint16_t start_reg, num_regs; 
 public:  
  ModbusReadHoldingRegs();
  ~ModbusReadHoldingRegs();

  uint8_t param_len(){return 4;}
  uint8_t func(){return 3;}
  
  int issue_cmd(uint8_t addr, PROC_TAG ptag, uint8_t *resp_buff, uint16_t start_reg, uint8_t num_regs){
    this->start_reg = start_reg;
    this->num_regs  = (uint16_t)num_regs;
    return init_txn(addr, ptag, resp_buff);

  }

  uint16_t param(uint8_t _idx){

      //case 0 : return (uint16_t)

    switch(_idx){
    case 0: return  (uint16_t) (start_reg >> 8);
    case 1: return  (uint16_t) (start_reg & 0xff);
    case 2: return  (uint16_t) (num_regs >> 8);
    case 3: return  (uint16_t) (num_regs  & 0xff);
    default :  return 0xffff;
    }
  }
  
  int _rx_byte(uint8_t _rxb){

    if (get_idx() == 2)  // expect resp nbytes
      resp_len = _rxb + 3; 
    return 0;
  }

};


//support stream access command only

//const uint8 read_dev_id_codes[] =  {  0x2b, 0x0e, 0x01, 0x00};
/*


typedef struct  {
  uint8 mei_type;
  uint8 read_id_code;
  uint8 object_id;
} ModbusReadDeviceIdReqObj;






typedef struct  {
  const uint8 func_code    = 0x2b;
  const uint8 mei_type     = 0x0e;
  const uint8 read_id_code = 0x01;
  uint8 more_follows;   //expect 00
  uint8 next_object_id; // expect 00
  uint8 number_of_objects; //expect 3
  uint8 obj0_id; //expect 0 VendorName
  uint8 obj0_len; //expect 16
  uint8 obj0[16]; //
  uint8 obj1_id; //expect 1 VendorName
  uint8 obj1_len; //expect 16
  uint8 obj1[9]; //


} ModbusRespReadDeviceID;
*/



class ModbusReadDeviceID : public ModbusHandler {

 public:  
  static const uint8_t modbus_read_devid_params[3]; // =  { 0x0e , 0x01, 0x00 };
 ModbusReadDeviceID();
  ~ModbusReadDeviceID(); 
  static uint8_t resp_obs, ob_num, ob_len,ob_idx,ob_st;


 uint8_t param_len(){return 3;}
 uint8_t func(){return 0x2b;}
  
  int _rx_byte(uint8_t _rxb){

    if (get_idx() == 7)  // num obs
      resp_obs = _rxb;
    else if (resp_obs != 0) {
      if (ob_num < resp_obs){
        if(ob_st == 0) { // obj id , expect count 0,1,2
          if(_rxb != ob_num)
            return -55; // random err code FIXME
          else {
            ob_st++;
            return 0;
          }
        }
        else if(ob_st == 1) { // ob len
          if (ob_num == (resp_obs - 1)) { // last ob len
            resp_len = get_idx() + _rxb + 1;
            return 0;
          }
          ob_len = _rxb;
          ob_idx = 0;
          ob_st++;
          return 0;
        } else if (ob_st == 2) {
          if (ob_idx < ob_len){
            ob_idx++;
          }
          if(ob_idx == ob_len) {
            ob_num++;
            ob_idx= 0;
            ob_st = 0;
          }
          return 0;

        }

      }
    }
    return 0;
  }

  uint16_t param(uint8_t idx){
    if (idx < sizeof(ModbusReadDeviceID::modbus_read_devid_params))
      return (uint16_t)ModbusReadDeviceID::modbus_read_devid_params[idx];
    else
      return 0xffff;

  }


  int issue_cmd(uint8_t addr, PROC_TAG ptag, uint8_t *resp_buff){
    resp_obs = 0; ob_num = 0; ob_len = 0; ob_idx = 0; ob_st = 0;
    return init_txn(addr, ptag, resp_buff);

  }

};



// resp_maxlen can prevent response overflowing buffer ,  *resp_len is written with number of bytes received
//uint8 mrf_modbus_read_devid(uint8 addr, uint8 *resp_buff ,  uint8 *resp_len);


// init func defined in arch dep file mrf_modbus_<arch>.c



#endif
