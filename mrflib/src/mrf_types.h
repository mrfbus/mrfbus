/******************************************************************************
*
* Copyright (c) 2012-16 Gnusys Ltd
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
******************************************************************************/

#ifndef _MRF_TYPES_INCLUDED_
#define _MRF_TYPES_INCLUDED_
#include "stdint.h"
#include "stddef.h"
#include "mqueue.hpp"
#include <device.h> // for q depths

//  FIXME following should be retired - just use the portable ctypes in standard includes aliased here

#define uint32 uint32_t
#define int32  int32_t

#define uint16 uint16_t

#define int16 int16_t
#define uint8 uint8_t
#define int8  int8_t

// tag format - item in ackqueue
typedef struct{
  uint16 msgid;
  uint8 type;
  uint8 dest;
} ACK_TAG;


/*
typedef struct{
  uint8 cmd;
  uint8 bnum;
} PROC_TAG;
*/

typedef struct{
  uint8 cmd;
  uint8 bnum;
  uint8 param1;
  uint8 param2;
} PROC_TAG;


#define NULL_PROC_TAG {0xff,0xff,0xff,0xff}



// define types for ack and buff queues
typedef MQueue<ACK_TAG,ACK_QUEUE_DEPTH> AckQueue;   // used by system to queue acks : depth 4 should be more than enough for all apps , collisions are rare to impossible
typedef MQueue<uint8_t,TX_QUEUE_DEPTH> BuffQueue;  // BuffQueue used for interface tx queues
// sys f/g queue 

typedef MQueue<PROC_TAG,SYS_QUEUE_DEPTH> SysQueue;  // sys processing queue, cleared by mrf_foreground

typedef MQueue<uint8_t,APP_QUEUE_DEPTH> AppQueue;  // app_queue queues app signals



//#define uint uint16
typedef void  (*VFUNCPTR)(void);

#endif
