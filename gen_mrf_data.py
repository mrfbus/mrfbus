#!/usr/bin/env python3
import argparse
import os
import datetime
import base64, hashlib




def random_str(l,as_bytes=False):
    """ lash up random_str generator """
    seed = base64.b64encode(os.urandom(32+l))
    m = hashlib.sha256()
    tnonce = bytes("%d"%(datetime.datetime.now().microsecond), 'utf-8')
    m.update(tnonce)
    m.update(seed)
    if as_bytes:
        return base64.b64encode(m.digest())[:l]
    else:
        return( base64.b64encode(m.digest()).decode('utf-8')[:l])
    
    
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-chann_offset', required=True, type=int, default=0,help="radio channel offsets from nominal")
    parser.add_argument('-land_port', required=True, type=int, default=0,help="land port number")
    parser.add_argument('-test_port', required=True, type=int, default=0,help="test port number")
    parser.add_argument('-http_port', required=True, type=int, default=0,help="http port number")
    parser.add_argument('-cons_port', required=True, type=int, default=0,help="http port number")
    parser.add_argument('-mrfnet',   required=True, type=int, default=0,help="mrfnet")
    
    parser.add_argument('-app_name'  , required=True, type=str, default="NONE",help="application name/tag tp distinguish mrfland applications running on the same host")

    args = parser.parse_args()

    print (repr(args))

    # gen python data file with port numbers

    pfile = open('mrf_proj.py','w')
    pfile.write("APP_NAME  = '%s'\n"% args.app_name)
    pfile.write("MRFNET    = %d\n"% args.mrfnet)
    pfile.write("\n")
    pfile.write("LAND_PORT = %d\n"% args.land_port)
    pfile.write("HTTP_PORT = %d\n"% args.http_port)
    pfile.write("TEST_PORT = %d\n"% args.test_port)
    pfile.write("CONS_PORT = %d\n"% args.cons_port)

    # looking for coexistence of multiple deployments
    pfile.write("\n")
    pfile.write("sess_cookie = 'MRFSESSID_%s'\n"%args.app_name)
    pfile.write("public_cookie = 'MRFPUBID_%s'\n"%args.app_name)
    pfile.write("cookie_secret = '%s'\n"% random_str(32))
    
    pfile.close()
    
    
    akey = random_str(32,as_bytes=True)

    hfile = open('mrf_kdata.h','w')
    hfile.write("#include  <stdint.h>\n")

    hfile.write("#define MRF_CHANN_OFFSET  %d\n"%args.chann_offset)
    hfile.write("#define LAND_PORT         %d\n"%args.land_port)

    #hfile.write("extern const uint16_t  *_mrf_key;\n")

    hfile.close()

    cfile = open('mrf_kdata.c','w')
    cfile.write("#include  <stdint.h>\n")

    cfile.write("extern const uint16_t  _mrf_key[8] = {")

    wn = 0
    for i in range(16):
        if (i%2)== 0:
            val = akey[i]
        else:
            val = val + (akey[i] * 256)

            if (wn%4) == 0:
                cfile.write("\n   ")
            if i < 15:
                cfile.write("0x%04x,"%val)
            else:
                cfile.write("0x%04x"%val)
            wn += 1

    cfile.write("\n};\n")

    cfile.close()
