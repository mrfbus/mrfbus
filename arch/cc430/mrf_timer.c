#include "mrf_types.h"
#include "mrf_sys.h"
#include  <msp430.h>
#include <legacymsp430.h>
#include "cc430f5137.h"

static uint16_t one_shot_count, timer_a0_irq_cnt, oflow_cnt;
static PROC_TAG timer_ptag;

int mrf_timer_init(){
  SET_NULL_PTAG(timer_ptag);
  one_shot_count   = 0;
  oflow_cnt        = 0;
  timer_a0_irq_cnt = 0;
  return 0;
}

// count at 32kHz
int mrf_timer_callback(uint16_t count, PROC_TAG ptag){
  if (!IS_NULL_PTAG(timer_ptag)){  // last is still pending
    oflow_cnt++;
    return -1;
  }
  TA0CTL     = TASSEL0 | TACLR;  // stopped and cleared , clocking from ACLK
  TA0CCR0    = count;
  timer_ptag = ptag;
  TA0CCTL0   = CCIE;                          // CCR0 interrupt enabled
  TA0CTL     = TASSEL0 | MC0 ;  // count up
  return 0;
}


interrupt (TIMER0_A0_VECTOR) TIMER0_A0_ISR()
{
  timer_a0_irq_cnt++;
  TA0CTL     = TASSEL0 | TACLR;  // stopped and cleared , clocking from ACLK
  TA0CCTL0   = 0;  //mrf_wake();
  if (!IS_NULL_PTAG(timer_ptag)) {
    mrf_sys_queue_push(timer_ptag);
    SET_NULL_PTAG(timer_ptag);
    one_shot_count++;
    //if (mrf_wake_on_exit())
    __bic_SR_register_on_exit(LPM3_bits);
  }
}
