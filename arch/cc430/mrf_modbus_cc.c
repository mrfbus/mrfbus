/******************************************************************************
*
* Copyright (c) 2012-16 Gnusys Ltd
* Copyright (c) 2012-21 Steve Murphy , owners of gnusys.com
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
******************************************************************************/



#include "mrf_uart.h"
#include "mrf_sys.h"
#include "mrf_if.h"
#include "mrf_debug.h"
#include  <msp430.h>
#include <legacymsp430.h>
#include "cc430f5137.h"
#include "mrf_pinmacros.h"
#include "mrf_modbus.h"
#include "mrf_timer.h"

// Designed to run on Olimex CC5137 directly connected to Olimex MOD-485 via UEXT connector/ribbon cable

// Requires port map to swap TXD/RXD  ( see mrf_modbus_init below )

// _REN  - recieve_enable , active LOW
// _TE   - transmit_enable , active HIGH

#define _REN_PORT P1
#define _TE_PORT P1

#define _REN_BIT  7

#define _TE_BIT  4

#define _RXD_PORT P1
#define _TXD_PORT P1

#define _RXD_BIT  6
#define _TXD_BIT  5



static inline void channel2idle(){
  PINLOW(TE);
  PINHIGH(REN);
}

static inline void channel2tx(){
  PINHIGH(REN);
  PINHIGH(TE);
}


static inline void channel2rx(){
  PINLOW(TE);
  PINLOW(REN);
}



// modbus master, always TX first then RX for resp
// state controls half duplex link functionality
typedef enum {
  ST_IDLE = 0,
  ST_TX   = 1,
  ST_RX   = 2
} MRF_MODBUS_IF_STATE;



MRF_MODBUS_IF_STATE ifstate;

//int mrf_modbus_send(ModbusHandler *mhandler);

static ModbusHandler *mh;



static void set_linestate() {
  switch(ifstate) {
  case ST_TX: channel2tx()  ; break;
  case ST_RX: channel2rx()  ; break;
  default: channel2idle(); break;
  }
}


int uart_cc_disable_tx_int(){
  UCA0IE &= ~UCTXIE;  // disable this intr
  return 0;
}

int uart_cc_enable_tx_int(){
  UCA0IE &= ~UCTXIE;  // disable this intr
  UCA0IE |= UCTXIE;  //re-enable this intr
  // __bis_SR_register(GIE);   // 
  return 0;
}


int uart_cc_disable_rx_int(){
  UCA0IE &= ~UCRXIE;  // disable this intr
  return 0;
}

int uart_cc_enable_rx_int(){
  UCA0IE &= ~UCRXIE;  // disable this intr
  UCA0IE |= UCRXIE;  //re-enable this intr
  //__bis_SR_register(GIE);   // 
  return 0;
}

void start_turn_around(){
  PROC_TAG ptag;
  ptag.cmd = _MRF_APP_QCMD_BASE + APP_CMD_MODBUS_TURN_AROUND;
  mrf_timer_callback(50,ptag);  // delay ptag exe
}

// need to call this from sys_queue using timer delay - some app code support required

int mrf_modbus_turn_around() { // got to RX and wait for response

  // FIXME delay until UCBUSY goes inactive

  
  //while(UCA0STAT & 0x0001)
  //  __delay_cycles(100);

  //__delay_cycles(200);
  mh->start_rx();
  ifstate = ST_RX;
  set_linestate();
  uart_cc_disable_tx_int();
  uart_cc_enable_rx_int();
  return 0;
  //UCA0IE |= UCRXIE;        // enable RX interrupt

 }

int dummy_func(){
  return 0;
}

  // set us to handle modbus driving

//ModbusHandler::hw_handler = mrf_modbus_send;  //set_handler(1);


int mrf_modbus_init(MODBUS_BAUD baud, MODBUS_PARITY parity){

  uint8_t par_mask;
  ifstate   = ST_IDLE;

  mh = NULL;

  switch (parity)
    {
    case PAR_ODD  : par_mask = UCPEN; break;
    case PAR_EVEN : par_mask = UCPEN | UCPAR;  break;
    default       : par_mask = 0;  break;
    }
  
  //
  UCA0CTL1 |= UCSWRST;                      // **Put state machine in reset**

  // SWAP RXD and TXD to match MOD-485 UEXT wiring
#if 1  // swap rxd txd

  PMAPPWD = 0x02D52;                        // Get write-access to port mapping
  P1MAP6 = PM_UCA0RXD;                      // Map UCA0RXD output to P1.6
  P1MAP5 = PM_UCA0TXD;                      // Map UCA0TXD output to P1.5
  PMAPPWD = 0;                              // Lock port mapping registers
#endif
  // set up data registers for wire control before configuring pins
  channel2idle();

  OUTPUTPIN(TE);
  OUTPUTPIN(REN);
  PULLUP(TE); // shouldn't be needed , desperate, maybe damaged output driver on a test rig card when tried this?

  
  //  P1SEL |= BIT5 + BIT6; // uart function
  OUTPUTPIN(TXD);
  INPUTPIN(RXD);

  FUNCSEL(TXD);
  FUNCSEL(RXD);


  //P1DIR |= BIT6;  // tx out
  //P1DIR &= ~BIT5;  // rx in

  set_linestate();
  UCA0CTL0  = par_mask ;
 
  UCA0CTL1 |= UCSSEL__SMCLK;                     // CLK = ACLK

  // following Table 22-4 cc430f5137ug_rev_e.pdf
  // support so far for 4 and 8MHz main clocks for 9600 and 19200 baud
  // expand as necessary
  
  switch(baud){
  case BAUD_9600:
    switch (MRF_CLK_MHZ ){
    case 4:
      UCA0BRW =   436;
      UCA0MCTL = UCBRS_7+UCBRF_0;
      break;
    case 8:
      UCA0BRW =   873;
      UCA0MCTL = UCBRS_7+UCBRF_0;
      break;
    default:
      return -1;
    }
    break;
  case BAUD_19200:
    switch (MRF_CLK_MHZ ){
    case 4:
      UCA0BRW =   218;
      UCA0MCTL = UCBRS_4+UCBRF_0;
      break;
    case 8:
      UCA0BRW =   436;
      UCA0MCTL = UCBRS_7+UCBRF_0;
      break;
    default:
      return -1;
    }
    break;
  default:
    return -1;
  }
  //ctrl1 = *(volatile uint8_t *)UCA0CTLW0_;

  //ctrl1 &=  ~UCSWRST;
  
  //*(volatile uint8_t *)UCA0CTLW0_ = *(volatile uint8_t *)UCA0CTLW0_ &  (uint8_t)(~UCSWRST);
  uart_cc_disable_tx_int();
  uart_cc_disable_rx_int();
  
  UCA0CTL1 &= ~UCSWRST;                     // **Initialize USCI state machine**
  //*(volatile uint8_t *)UCA0CTLW0_ = 0x80;

  //UCA0CTL1 = 0x80;
  //UCA0IE |= UCRXIE;        // enable RX interrupt
  //__bis_SR_register(GIE);  // leave for system to set
  return 0;
}


  
static int _tx_rdy_cnt;




// buff and len refer to


uint16_t mbus_wake_debug_cnt;


#define UCARXBUFF *((volatile uint8_t *)0x05CC)

uint8_t tbd;


//ModbusHandler *currh;

// called by foreground, should be non blocking
int ModbusHandler::hw_handler(ModbusHandler *mhandler){

  // if (mh == NULL)
  //  return 0xf022;
  PROC_TAG tptag;

  mh = mhandler;
  
  if (ifstate != ST_IDLE) {
    mrf_debug(1,"ERROR: uart_if_send_func found if busy");
    uart_cc_disable_tx_int();
    uart_cc_disable_rx_int();
    //enable_tx_int();
    //disable_tx_int();
  }
  
  mbus_wake_debug_cnt= 0;
  _tx_rdy_cnt = 0;

  ifstate = ST_TX;
  set_linestate();
  tbd = UCARXBUFF;

  // delay start tx after setting line drivers above
  
  tptag.cmd = _MRF_APP_QCMD_BASE + APP_CMD_MODBUS_START_TX;

  mrf_timer_callback(16,tptag);  // delay ptag exe

  
  /*
  if ((UCA0STAT & 0x1) == 0) // not busy , write first TXB
    UCA0TXBUF  = mh->tx_byte();
  uart_cc_enable_tx_int();
  */

  return 0;
}

int mrf_modbus_start_tx() { // start tx ( after timer delay)
  if ((UCA0STAT & 0x1) == 0) // not busy , write first TXB
    UCA0TXBUF  = mh->tx_byte();
  uart_cc_enable_tx_int();

  return 0;
}


void rx_newpacket(){
  //  rxstate.bindex = 0;
}






void rx_error(){
       UCA0IE &= ~UCRXIE;         // disable RX ready interrupt - shouldn't need this
 
}


//#define LP_9600
unsigned int _uart_rx_int_cnt;
unsigned int _uart_tx_int_cnt;



static uint8 last_rx;

static volatile uint8 _db11;
void dbg_break(uint8 rxb){
  _db11 = rxb;
}

static uint8  _rx_byte(){
  last_rx = UCA0RXBUF;
  return last_rx;
}

void _dbg_pkt_sent(UART_CSTATE *txstate){


}


 static uint8 _rxb;

static uint16_t _txb;
static int rx_stat;

static uint8_t rx_dbg_buff_idx,rx_dbg_buff[16];


interrupt (USCI_A0_VECTOR) USCI_A0_ISR()
{
  switch(UCA0IV)
  {
  case 0:break;                             // Vector 0 - no interrupt
  case 2:                                   // Vector 2 - RXIFG
    _uart_rx_int_cnt++;
    _rxb = _rx_byte();

    if (rx_dbg_buff_idx < 16){  // debug
      rx_dbg_buff[rx_dbg_buff_idx++] = _rxb;
    }
    rx_stat = mh->rx_byte(_rxb);

    if ( rx_stat == 0)
      UCA0IE |= UCRXIE;         // re-enable RX ready interrupt
    else if(rx_stat == 1){ // should be able to rely on modbus handler doing this TBD
      mrf_wake();
    }
    else {
      rx_error();
    }
    break;
  case 4:                                   // Vector 4 - TXIFG
    _uart_tx_int_cnt++;

    _txb = mh->tx_byte();

    if(_txb & 0x8000) {
      UCA0IE &= ~UCTXIE;
      start_turn_around();
      rx_dbg_buff_idx =0;
    }

    else {

      UCA0TXBUF = (uint8_t)_txb;

      UCA0IE |= UCTXIE;  //re-enable this int

    }
    break;
  default: break;
  }

   if (mrf_wake_on_exit())
     __bic_SR_register_on_exit(LPM3_bits);

}
