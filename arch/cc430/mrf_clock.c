/******************************************************************************
*
* Copyright (c) 2012-16 Gnusys Ltd
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
******************************************************************************/

//#define MCLK_8MHZ_DCO
//MCLK_XT2
#include  <msp430.h>
#include <legacymsp430.h>
#include <mrf_types.h>

// MCLK_8MHZ_DCO

#ifndef MRF_CLK_MHZ
#error "MRF_CLK_MHZ is not defined"
#endif



const uint8_t MRF_CLK_MHZ_build = MRF_CLK_MHZ;

int  mrf_init_clock(){

#ifdef CLOCK_DEFAULT
return 0;

#else
P5SEL = 3; // enable XT1 oscillator

UCSCTL3 = SELREF__XT1CLK + FLLREFDIV_0;

__bis_SR_register(SCG0);                  // Disable the FLL control loop
UCSCTL0 = 0x0000;                         // Set lowest possible DCOx, MODx

switch (MRF_CLK_MHZ ){
 case 2:
   UCSCTL1 = DCORSEL_2;                      // Select DCO range 2MHz operation
   UCSCTL2 = FLLD_1 + 63;                    // Set DCO Multiplier for 2MHz
   break;
 case 4:
   UCSCTL1 = DCORSEL_3;                      // Select DCO range 4MHz operation   
   UCSCTL2 = FLLD_1 + 127;                   // Set DCO Multiplier for 4MHz
  break;
 case 8:
   UCSCTL1 = DCORSEL_4;                      // Select DCO range 8MHz operation
   UCSCTL2 = FLLD_1 + 255;                   // Set DCO Multiplier for 8MHz
   break;
 case 12:  //11927552 Hz
   UCSCTL1 = DCORSEL_5;                      // Select DCO range 16MHz operation
   UCSCTL2 = FLLD_1 + 363;                   // Set DCO Multiplier for 16MHz
   break;
 case 16:
   UCSCTL1 = DCORSEL_5;                      // Select DCO range 16MHz operation
   UCSCTL2 = FLLD_1 + 511;                   // Set DCO Multiplier for 16MHz
   break;
 case 20: // 20217856 Hz
   UCSCTL1 = DCORSEL_6;                      // Select DCO range 16MHz operation
   UCSCTL2 = FLLD_1 + 511;                   // Set DCO Multiplier for 16MHz
 default:
   return -1; 
}




 __bic_SR_register(SCG0);                  // Enable the FLL control loop
 for(int x=0 ; x<255;x++)
   __delay_cycles(1000);
 // Loop until XT1,XT2 & DCO fault flag is cleared
 do
   {
     UCSCTL7 &= ~(XT2OFFG + XT1LFOFFG + XT1HFOFFG + DCOFFG);
     // Clear XT2,XT1,DCO fault flags
     SFRIFG1 &= ~OFIFG;                      // Clear fault flags
   }while (SFRIFG1&OFIFG);                   // Test oscillator fault flag


 UCSCTL4 = SELA__REFOCLK + SELS__DCOCLKDIV + SELM__DCOCLKDIV;
 // UCSCTL4 = ucsctl4_val;

#endif // not CLOCK_DEFAULT

 return 0;
 
}
