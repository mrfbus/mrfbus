/******************************************************************************
*
* Copyright (c) 2012-16 Gnusys Ltd
* Copyright (c) 2012-2021 Steve Murphy and the owners of gnusys.com
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
******************************************************************************/
#include "cc430f5137.h"
#include "stdint.h"

#ifndef _MRF_CRC_INCLUDED_
#define _MRF_CRC_INCLUDED_

// note this  CRC16-CCITT  - you cannot do MODBUS CRC with this code as HW accelerator has fixed polynomial 

void crc_init(){
  CRCINIRES = 0xffff;
}

// add d8 item to crc calc
void crc_d8(uint8_t d8){
  CRCDIRB_L = d8;
}


 uint16_t crc_res(){
   return  CRCINIRES;
}

#endif
