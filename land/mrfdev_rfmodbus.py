#!/usr/bin/env python
'''  Copyright (c) 2012-17 Gnusys Ltd

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

from mrf_sens import MrfSens
from mrf_dev  import MrfDev
import datetime
import ctypes
from mrf_structs import *
from core_tests import mrf_cmd_app_test
from math import sqrt
from mrflog import mrflog
from collections import deque
from mrf_sens_modbus_id import MrfSensModbusId
from mrf_sens_relay import MrfSensRelay
#from mrf_sens_electric import MrfSensElectric
from mrfdev_rfmodtc import PktRfmodtcState, MrfSensTempX100
from mrf_sens_float import *
#from mrfdev_pt1000 import PktRelayState, MrfSensPtRelay



mrf_app_cmd_test = 128
mrf_cmd_led_on   = 129
mrf_cmd_led_off   = 130
mrf_cmd_get_relay   = 131
mrf_cmd_set_relay   = 132
mrf_cmd_read_state   = 133
mrf_cmd_read_device_id  = 134
mrf_cmd_read_hold_regs  = 135
mrf_cmd_read_meter      = 136


RfmodbusAppCmds = {
    mrf_cmd_app_test : {
        'name'  : "APP_TEST",
        'param' : None,
        'resp'  : PktTimeDate
    },
    mrf_cmd_led_on : {
        'name'  : "LED_ON",
        'param' : None,
        'resp'  : PktRelayState
    },
    mrf_cmd_get_relay : {
        'name'  : "LED_OFF",
        'param' : None,
        'resp'  : PktRelayState
    },
    mrf_cmd_get_relay : {
        'name'  : "GET_RELAY",
        'param' : PktRelayState,
        'resp'  : PktRelayState
    },
    mrf_cmd_set_relay : {
        'name'  : "SET_RELAY",
        'param' : PktRelayState,
        'resp'  : PktRelayState
    },

    mrf_cmd_read_state : {
        'name'  : "READ_STATE",
        'param' : None,
        'resp'  : PktRfmodtcState
    },


    mrf_cmd_read_device_id : {
        'name'  : "READ_DEVICE_ID",
        'param' : PktUint8,
        'resp'  : PktMaxResp
    },
    mrf_cmd_read_hold_regs : {
        'name'  : "READ_HOLD_REGS",
        'param' : PktModbusRead,
        'resp'  : PktMaxResp
    },
    mrf_cmd_read_meter : {
        'name'  : "READ_METER",
        'param' : PktUint8,
        'resp'  : PktModbusMeterState
    },

}


""" Schneider A9MEM2150 Modbus reg address """

A9MEM2150_REGS = {

    2999 : "amps",
    3027 : "volts",
    3053 : "kwatts",
    3067 : "kvar",
    3075 : "kva",
    3083 : "pfactor",
    3109 : "hertz"
    }
# create reversed lookup
A9MEM2150_FUNCS = {v: k for k, v in A9MEM2150_REGS.items()}

# essentially map from sensor chan to func for power variables
POWER_FIELDS = ['kwatts','kvar', 'kva']

class DevRfModbus(MrfDev):

    _capspec = {
        'temp'     : MrfSensTempX100,
        'relay'    : MrfSensRelay,
        'devid'    : MrfSensModbusId,
        'volts'    : MrfSensFloatVolts,
        'kpower'   : MrfSensFloatKPower,
        'pfactor'   : MrfSensFloatPFactor,
        'amps'     : MrfSensFloatAmps,
        'hertz'    : MrfSensFloatHertz
    }
    _cmdset = RfmodbusAppCmds

    def init(self):
        self.filt_ind = 0
    def app_packet(self, hdr, param , resp):
        mrflog.info("%s app_packet type %s"%(self.__class__.__name__, type(resp)))

        mrflog.info("%s app_packet, hdr %s param %s resp %s"%(self.__class__.__name__,
                                                              repr(hdr), repr(param), repr(resp)))
        now = datetime.datetime.now()
        td =  PktTimeDate()
        td.set(now)

        if param.type == mrf_cmd_read_state:

            for ch in range(len(resp.tempX100)):
                #mrflog.debug("chan %s milliohms %d type %s"%(ch, resp.milliohms[ch], type(resp.milliohms[ch])))
                inp = { 'date' : resp.td,
                        'tempX100' : resp.tempX100[ch]
                }
                self.caps['temp'][ch].input(inp)


            for ch in range(len(self.caps['relay'])):
                (resp.relay_state >> ch) & 0x01
                inp = { 'date' : resp.td,
                        'relay' :  (resp.relay_state >> ch) & 0x01
                }
                self.caps['relay'][ch].input(inp)


        elif  param.type == mrf_cmd_set_relay or param.type == mrf_cmd_get_relay:
            inp = { 'date' : td,
                    'relay' :  resp.val
            }
            self.caps['relay'][resp.chan].input(inp)

        elif param.type == mrf_cmd_read_device_id:
            mrflog.info("DevRfModbus mrf_cmd_read_device_id app_packet, hdr %s param %s resp %s \n resp len %d"%(repr(hdr), repr(param), repr(resp),len(resp)))
            inp = { 'date' : td,
                    'resp'    :  resp.value
            }
            self.caps['devid'][0].input(inp)


        elif param.type == mrf_cmd_read_hold_regs:
            mrflog.info("DevRfModbus mrf_cmd_read_hold_regs app_packet, hdr %s param %s resp %s \n resp len %d"%(repr(hdr), repr(param), repr(resp),len(resp)))

            resp = resp.value  # cheap way to copy code that was in sensor previously, below
            rbytes = bytes(resp)
            hdr = PktModbusReadRespHdr()
            hdr.load(rbytes)
            mrflog.debug(repr(hdr))

            rbytes = rbytes[len(hdr):]

            mrflog.debug("remaining %d : %s"%(len(rbytes),repr(rbytes)))

            fval = struct.unpack('>f',rbytes[0:4])[0]
            mrflog.debug("fval : %f",fval)


            if hdr.start_addr not in A9MEM2150_REGS:
                mrflog.warn("reg addr %d not found in A9MEM2150_REGS",hdr.start_addr)
                return

            sn = A9MEM2150_REGS[hdr.start_addr]
            
            inp = { 'date' : td,
                    'value':  fval
            }
            
            self.caps[sn][0].input(inp)


        elif param.type == mrf_cmd_read_meter:
            mrflog.debug("DevRfModbus mrf_cmd_read_meter app_packet, hdr %s param %s resp %s \n resp len %d"%(repr(hdr), repr(param), repr(resp),len(resp)))
            inp = { 'date' : td,
                    'value':  0.0
            }

            

            for sn in A9MEM2150_FUNCS:
                
                inp['value'] = resp[sn]
                mrflog.debug("%s =  %.2f"%(sn,inp['value']))

                if sn in POWER_FIELDS:
                    cn = POWER_FIELDS.index(sn)
                    self.caps['kpower'][cn].input(inp)
                else:
                    self.caps[sn][0].input(inp)


            #update temp sensor
            inp = { 'date' : td,
                        'tempX100' : resp.tempX100
                }
            self.caps['temp'][0].input(inp)

            # update relay sensors
            """
            for ch in range(len(self.caps['relay'])):
                (resp.relay_state >> ch) & 0x01
                inp = { 'date' : td,
                        'relay' :  ((resp.relay_state >> ch) & 0x01) 
                }
                self.caps['relay'][ch].input(inp)
            """
            
            """
            resp = resp.value  # cheap way to copy code that was in sensor previously, below
            rbytes = bytes(resp)
            hdr = PktModbusReadRespHdr()
            hdr.load(rbytes)
            mrflog.warn(repr(hdr))

            rbytes = rbytes[len(hdr):]

            mrflog.warn("remaining %d : %s"%(len(rbytes),repr(rbytes)))

            fval = struct.unpack('>f',rbytes[0:4])[0]
            mrflog.warn("fval : %f",fval)


            if hdr.start_addr not in A9MEM2150_REGS:
                mrflog.warn("reg addr %d not found in A9MEM2150_REGS",hdr.start_addr)
                return

            sn = A9MEM2150_REGS[hdr.start_addr]
            
            inp = { 'date' : td,
                    'value':  fval
            }
            
            self.caps[sn][0].input(inp)
            """
