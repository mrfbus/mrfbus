#! /usr/bin/env python

from tornado.options import parse_command_line  #FIXME move mrfland_server.py

import mrfland

#from mrflog import mrflog, mrf_log_init
from mrfdev_pt1000 import Pt1000Dev
from mrfdev_host import MrfDevHost
from mrfdev_rfmodbus import DevRfModbus
from mrfvdev_vic_modbus import VDevVicModbus
from mrfvdev_sysmon import VDevSysMon


from mrfland_weblet_elec_meter import MrfLandWebletElecMeter
from mrfland_weblet_temps  import MrfLandWebletTemps
from mrfland_weblet_relays import MrfLandWebletRelays
from mrfland_weblet_devs   import MrfLandWebletDevs
from mrfland_weblet_store  import MrfLandWebletStore
from mrfland_weblet_hot_water  import MrfLandWebletHotWater
from mrfland_weblet_rad_pump import MrfLandWebletRadPump
from mrfland_weblet_ufh import MrfLandWebletUFH
from mrfland_weblet_history import MrfLandWebletHistory
from mrfland_weblet_victron  import MrfLandWebletVictron
from mrfland_weblet_tfr_sw import MrfLandWebletTfrSw
from mrfland_weblet_system import MrfLandWebletSystem
from mrfland_server import MrflandServer
import install

import mrf_proj

from mrfland_regmanager import MrflandRegManager

if __name__ == '__main__':
    #mrf_log_init()

    parse_command_line()


    rm = MrflandRegManager( {
        'http_port'       : mrf_proj.HTTP_PORT,
        'db_uri'          : install.db_uri
    })

    MrfDevHost(rm, "host", 1)

    Pt1000Dev(rm, "pt1000_boiler_room", 2,
              {
                  'temp' : ["ACC_100", "ACC_60", "ACC_30", "MAIN_MIX", "ACC_RET" , "UFH_FLOW", "UFH_RET"],
                  'relay' : ["UFH_PUMP", "UNUSED_PUMP", "ACC_HEAT"]
              })

    Pt1000Dev(rm, "pt1000_kitchen"  , 4,
               {
                   'temp' : ["DHW1_100",  "HB1_FLOW", "HB1_RET", "RAD1_RET", "DHW1_HX_RET",  "HB1_AMBIENT","LOUNGE_AMBIENT"],
                   'relay' : ["RAD1_PUMP", "DHW1_HX_PUMP" , "DHW1_HEAT","TFR1_SW"]
               })

    Pt1000Dev(rm, "pt1000_guest"  , 6,
               {
                   'temp' : ["DHW2_100",  "DHW2_30", "HB2_FLOW", "HB2_RET", "DHW2_HX_RET",  "HB2_AMBIENT","OUTSIDE_AMBIENT"],
                   'relay' : ["RAD2_PUMP", "DHW2_HX_PUMP" , "DHW2_HEAT"]
               })


    # RF devices

    DevRfModbus(rm, "rfmodbus_40"  , 0x40,
               {
                   'relay' : ["MODBUS_LED"],
                   'temp'  : ["RFMOD_AMBIENT"],
                   'devid' : ["MODBUS_HEAD1"],
                   'volts' : ["VOLTS_1"],
                   'kpower': ["KWATTS_1","KVAR_1","KVA_1"],
                   'amps'    : ["AMPS_1"],
                   'pfactor' : ["PFACTOR_1"],
                   'hertz' : ["HERTZ_1"]
               })

    """
    DevRfModbus(rm, "rfmodbus_20"  , 0x20,
               {
                   'relay' : ["MODBUS2_LED"],
                   'temp'  : ["RFMOD2_AMBIENT"],
                   'devid' : ["MODBUS_HEAD2"],
                   'volts' : ["VOLTS_2"],
                   'kpower': ["KWATTS_2","KVAR_2","KVA_2"],
                   'amps'    : ["AMPS_2"],
                   'pfactor' : ["PFACTOR_2"],
                   'hertz' : ["HERTZ_2"]
               })
    """

    VDevVicModbus(rm, "VICV", 0x10000,
                  {
                         'power' : ["VICV_AC_IN","VICV_AC_OUT", "VICV_BATT_POWER","VICV_PV_DC_POWER", "VICV_CHARGER_POWER","VICV_DC_SYS_POWER"],
                         'volts'  : ["VICV_BATT_VOLTS","VICV_PV_VOLTS","VICV_VOLTS_IN","VICV_VOLTS_OUT"],
                         'amps'   : ["VICV_BATT_AMPS","VICV_PV_DC_AMPS","VICV_PV_AMPS"],
                         'percent': ["VICV_BATT_CHARGE"],
                         'temp'   : ["VICV_BATT_TEMP"],
                         'ok'  : ["VICV_INPUT_POWER_OK"],
                         'state'  : ["VICV_CHARGE_STATE", "VICV_INPUT_SRC", "VICV_MPPT_MODE","VICV_VE_STATE"],
                         'mode' : ["VICV_MPPT_ST"]
                     }
    )


    VDevSysMon(rm, "SYSMON", 0x10000,
                  {
                         'percent' : ["SYSMON_PCPU","SYSMON_PMEM"],
                         'kb'  : ["SYSMON_RSS","SYSMON_SZ","SYSMON_VSZ"],
                         'started'   : ["SYSMON_STARTED"],
                         'uptime'   : ["SYSMON_UPTIME"]
                     }
    )
        

    MrfLandWebletElecMeter(rm,
                       {
                           'tag'     : 'ELEC',
                           'label'   : 'Electric',
                           'sensors' : { 'volts'  : ['VOLTS_1'],
                                         'kpower' : ['KWATTS_1','KVAR_1','KVA_1'],
                                         'amps'   : ['AMPS_1'],
                                         'pfactor'   : ['PFACTOR_1'],
                                         'hertz'  : ['HERTZ_1']
                                         }
                       })
    """
    MrfLandWebletElecMeter(rm,
                       {
                           'tag'     : 'ELEC2',
                           'label'   : 'Electric2',
                           'sensors' : { 'volts'  : ['VOLTS_2'],
                                         'kpower' : ['KWATTS_2','KVAR_2','KVA_2'],
                                         'amps'   : ['AMPS_2'],
                                         'pfactor'   : ['PFACTOR_2'],
                                         'hertz'  : ['HERTZ_2']
                                         }
                       })

    """
    MrfLandWebletVictron(rm,
                        {
                            'tag'  : 'VIC01',
                            'label': 'Victron1',
                            'targ' : 'VICV'


                       })

    MrfLandWebletTfrSw(rm,
                         {
                             'tag'  : 'TFRSW',
                             'label': 'TfrSwitch',
                             'load' : 'VICV_AC_OUT',
                             'grid' : 'KWATTS_1',
                             'battery' : 'VICV_BATT_CHARGE',
                             'switch' : 'TFR1_SW'

                         })



    MrfLandWebletStore(rm,
                    {
                        'tag'        : 'STORE',
                        'label'      : 'Heatstore',
                        'acc_tag'    : 'ACC',
                        'acc_litres' : 2200

                    })

    MrfLandWebletUFH(rm,
                         {
                             'tag'        : 'UFH1',
                             'label'      : 'Lounge',
                             'period'     : 'UFH',
                             'ambient'    : 'LOUNGE_AMBIENT',
                             'outside'    : 'OUTSIDE_AMBIENT',
                             'pump'       : 'UFH_PUMP',
                             'storesens'  : 'ACC_100',
                             'flowsens'   : 'UFH_FLOW',
                             'retsens'    : 'UFH_RET'
                         })


    MrfLandWebletRadPump(rm,
                         {
                             'tag'        : 'RAD1',
                             'label'      : 'Main Rads',
                             'rad'        : 'RAD1',
                             'pump'       : 'RAD1_PUMP',
                             'storesens'  : 'ACC_100',
                             'flowsens'   : 'HB1_FLOW',
                             'retsens'    : 'RAD1_RET'
                         })

    MrfLandWebletRadPump(rm,
                         {
                             'tag'        : 'RAD2',
                             'label'      : 'Guest Rads',
                             'rad'        : 'RAD2',
                             'pump'       : 'RAD2_PUMP',
                             'storesens'  : 'ACC_100',
                             'flowsens'   : 'HB2_FLOW',
                             'retsens'    : 'HB2_RET'
                         })


    MrfLandWebletHotWater(rm,
                          {
                              'tag'        : 'DHW1',
                              'label'      : 'Main Hot Water',
                              'rad'        : 'RAD1',
                              'acctop'     : 'ACC_100',
                              'heatbox'    : 'HB1',
                              'litres'     : 200

                          },
                          {
                              'target_temp': 60.0,
                              'delta_targ_rx' : 8.0,
                              'min_wait_hours' : 16.0,
                              'enabled'  : False
                          })

    MrfLandWebletHotWater(rm,
                          {
                              'tag'        : 'DHW2',
                              'label'      : 'Guest Hot Water',
                              'rad'  : 'RAD2',
                              'acctop'     : 'ACC_100',
                              'heatbox'    : 'HB2',
                              'litres'     : 200
                          },
                          {
                              'target_temp'    : 60.0,
                              'delta_targ_rx'  : 8.0,
                              'min_wait_hours' : 16.0,
                              'enabled'  : False
                          })


    MrfLandWebletTemps(rm,
                       {
                           'tag'  : 'TEMPS',
                           'label': 'Temperatures'
                       })


    MrfLandWebletRelays(rm,
                        {
                            'tag':'RELAYS',
                            'label':'Relays'
                        }
    )

    MrfLandWebletDevs(rm,
                       {
                           'tag'  : 'DEVS',
                           'label': 'Devices'
                       })

    MrfLandWebletHistory(rm,
                    {
                        'tag'        : 'HISTORY',
                        'label'      : 'History',
                        'start_of_records' : {
                            'month' : 12,
                            'year'  : 2017
                            },
                        'graphs': {
                            'Ambient' : { "temp" :["LOUNGE_AMBIENT","OUTSIDE_AMBIENT"]},
                            'Store'   : { "temp" :["ACC_100","ACC_60","ACC_30","ACC_RET"]},
                            'Energy'  : { "temp" :["ACC_100","ACC_60","ACC_30","OUTSIDE_AMBIENT"]},
                            'DHW1'    : { "temp" :["DHW1_100","HB1_FLOW","DHW1_HX_RET","ACC_100"],
                                          "relay" :["DHW1_HEAT","DHW1_HX_PUMP","RAD1_PUMP"]
                                         },
                            'PV'     :  {"power": ["VICV_PV_DC_POWER"],
                                         "mode" : ["VICV_MPPT_ST"]
                                         },
                            "Battery" : {"percent": ["VICV_BATT_CHARGE"],
                                         "volts"  : ["VICV_BATT_VOLTS"]
                                         }
                        }

                    })
    MrfLandWebletSystem(rm,
                        {
                            'tag'  : 'SYSTEM',
                            'label': 'System',
                            'targ' : 'SYSMON'


                       })   

    ml =  MrflandServer(rm,
                        {
                            'mrfbus_host_port': mrf_proj.LAND_PORT,
                            'mrf_netid'       : mrf_proj.MRFNET ,
                            'tcp_test_port'   : mrf_proj.TEST_PORT ,
                            'console'         : mrf_proj.CONS_PORT

                        })
