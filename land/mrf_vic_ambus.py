#!/home/spm/venv3/bin/python3

#/home/spm/share/eng/projects/msp/mrfbus/venv/bin/python

"""
Async modbus TCP to read victron
"""
#from pymodbus.constants import Defaults
from pymodbus.constants import Endian
from pymodbus.client.tcp import AsyncModbusTcpClient as ModbusClient
from pymodbus.payload import BinaryPayloadDecoder

#from pymodbus.register_read_message import ReadInputRegistersResponse


from collections import OrderedDict

import asyncio
#Defaults.Timeout = 25
#Defaults.Retries = 5

import pdb

class MbValue(object):
    __rtypes__ = {
        'uint16' : { 'count' : 1, 'decode' : 'decode_16bit_uint'} ,
        'int16'  : { 'count' : 1, 'decode' : 'decode_16bit_int'} ,
        'uint32' : { 'count' : 2, 'decode' : 'decode_32bit_uint'} ,
        'int32'  : { 'count' : 2, 'decode' : 'decode_32bit_int'}
    }
    def __init__(self,name, unit, regnum,slaveid, rtype, scale):
        self.name   = name
        self.regnum = regnum
        self.slaveid = slaveid
        self.unit   = unit
        if not rtype in self.__rtypes__:
            print("ERROR invalid rtype %s for MbValue %s"%(rtype,name))
            return
        self.rtype  = self.__rtypes__[rtype]
        self.scale  = scale

    async def value(self,client):
        #pdb.set_trace()
        result = await client.read_input_registers(self.regnum, self.rtype['count'],slave=self.slaveid)
        decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.BIG)
        #pdb.set_trace()
        dmeth   = getattr(decoder,self.rtype['decode'])
        v       = dmeth()
        if self.scale != 1:
            v = v/self.scale
        return v

    async def val_dict(self,client):
        """ return dict repr of self """
        rv = dict()
        rv['name'] = self.name
        rv['unit'] = self.unit
        rv['value'] = await self.value(client)
        return rv

    async def repr(self,client):
        v = await self.value(client)

        if type(self.unit) == dict:
            return "%-20s: %s"%(self.name,self.unit[v])
        if type(v) == float:
            return "%-20s: %.2f %s"%(self.name,v,self.unit)
        else:
            return "%-20s: %d %s"%(self.name,v,self.unit)

class VicModbusReader(object):

    def add_value(self,name,unit,rnum,sid,rt,scale):
        self.mbvalues[name]   = MbValue(name,unit  , rnum ,sid,rt,scale)

    def get_value(self,name):
        return  self.mbvalues[name].repr(self.client)

    async def get_val_dict(self,name):
        return  await self.mbvalues[name].val_dict(self.client)

    def __init__(self):
        self.mbvalues = OrderedDict()
        self.add_value('INPUT_SRC',
                  {0: 'unknown', 1 : 'grid', 2 : 'generator', 3: 'shore', 240: 'not connected'},
                       826,100,'int16',1)

        self.add_value('AC_OUT','W', 817,100,'uint16',1)
        #self.add_value('AC_consumption_L2','W', 818,'uint16',1)
        #self.add_value('AC_consumption_L3','W', 819,'uint16',1)

        self.add_value('AC_IN','W', 820,100,'int16',1)
        #self.add_value('Grid_L2','W', 821,'uint16',1)
        #self.add_value('Grid_L3','W', 822,'uint16',1)
        self.add_value('VOLTS_IN','V'  , 3,228,'uint16',10.0)
        self.add_value('VOLTS_OUT','V'  , 15,228,'uint16',10.0)

        self.add_value('BATT_VOLTS','V'  , 840,100,'uint16',10.0)
        self.add_value('BATT_AMPS','A'  , 841,100,'int16',10.0)
        self.add_value('BATT_POWER','W'  , 842,100,'int16',1)
        self.add_value('BATT_CHARGE','%', 843,100,'uint16',1)
        self.add_value('BATT_VOLTS_BATT','V'  , 259,225,'uint16',100.0)
        self.add_value('BATT_TEMP','C'  , 262,225,'uint16',10.0)

        self.add_value('CHARGE_STATE',
                  {0: 'idle', 1 : 'charging', 2 : 'discharging'},
                       844,100,'int16',1)


        self.add_value('VE_STATE',
                {
                      0: 'off',
                      1 : 'low power',
                      2 : 'fault',
                      3 : 'bulk',
                      4 : 'absorbtion',
                      5 : 'float',
                      6 : 'storage',
                      7 : 'equalize',
                      8 : 'passthrough',
                      9 : 'inverting',
                      10 : 'power assist',
                      11 : 'power supply',
                      252 : 'external control'
                  },
            31,228,'uint16',1)

        self.add_value('PV_DC_POWER','W'  , 850,100,'uint16',1)
        self.add_value('PV_DC_AMPS','A'  , 851,100,'int16',10)

        self.add_value('CHARGER_POWER','W'  , 855,100,'uint16',1)
        self.add_value('DC_SYS_POWER','W'  , 860,100,'int16',1)
        self.add_value('PV_VOLTS','V'     , 776,230,'uint16',100.0)
        self.add_value('PV_AMPS','A'     , 777,230,'int16',10.0)

        self.add_value('MPPT_MODE',
                       {0: 'off', 1 : 'limited', 2 : 'active', 255: 'unavail'},
                       791,230,'uint16',1)

        self.client = None
        #self.client = ModbusClient('nanopi', port='502')
        #await self.client.connect()




    async def connect(self):
         #client = await ModbusClient('nanopi', port='502')
        if not self.client:
            self.client = ModbusClient('nanopi', port='502')
 
        if not self.client.connected:
            await self.client.connect()

    def get_values(self):
        """ return dict of values"""
        rv = OrderedDict()
        for v in self.mbvalues.keys():
            print(self.get_value(v))

    async def get_all_dict(self):
        """ this is main async func run by weblet """
        await self.connect()
        rv = OrderedDict()
        for v in self.mbvalues.keys():
            rv[v] = await self.get_val_dict(v)
        # want to pass a simple grid power OK value here
        d = {'name':'INPUT_POWER_OK', 'unit' :'OK'}
        #mrflog.warn("rv : "+repr(rv))
        #mrflog.warn("rv['INPUT_SRC'] %d"%rv['INPUT_SRC']['value'])

        if rv['INPUT_SRC']['value'] in [ 0, 240] :
            d["value"] = 0
        else:
            d["value"] = 1
        rv['INPUT_POWER_OK'] = d

        # want numerical value for mppt mode for graphing

        d2 = {'name':'MPPT_ST', 'unit' :'MODE'}
        d2["value"] = rv['MPPT_MODE']['value']
        rv['MPPT_ST'] = d2



        return rv

    async def print_values(self):
        for v in self.mbvalues.keys():
            vl = await self.get_value(v)
            print(vl)


async def run():
    mbr = VicModbusReader()
    resp = await mbr.client.connect()
    print("await over")
    await mbr.print_values()

    da = await mbr.get_all_dict()
    print("dict : "+repr(da))
    print ("run finished")
    return resp


if __name__ == '__main__':


    asyncio.run(run())

#pdb.set_trace()

#mbc.print_values()

"""
result = client.read_input_registers(840, 2)
decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big)
voltage=decoder.decode_16bit_uint()
print("Battery voltage: {0:.2f}V".format(voltage/10.0))


result = client.read_input_registers(3, 2)
decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big)
voltage=decoder.decode_16bit_uint()
print("Input voltage (phase 1): {0:.2f}V".format(voltage/10.0))
"""

## input registers
"""
print("input registers")
for ra in range(1,5000):
    result = client.read_input_registers(ra, 2)
    if type(result) == ReadInputRegistersResponse:
        resp = "OK"
        print("Addr %d : %s"%(ra,resp))
    else:
        resp = "ERR"
    #print("Addr %d : %s"%(ra,resp))
"""
"""
## holding registers
print("holding registers")

for ra in range(1,5000):
    result = client.read_holding_registers(ra, 2)
    if type(result) == ReadInputRegistersResponse:
        resp = "OK"
        print("Addr %d : %s"%(ra,resp))
    else:
        resp = "ERR"
    #print("Addr %d : %s"%(ra,resp))

"""
