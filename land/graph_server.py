#! /usr/bin/env python

from tornado.options import parse_command_line  #FIXME move mrfland_server.py

import mrfland

#from mrflog import mrflog, mrf_log_init
from mrfdev_host import MrfDevHost

from mrfland_weblet_devs   import MrfLandWebletDevs

from mrfland_weblet_history import MrfLandWebletHistory
from mrfland_weblet_tst import MrfLandWebletTst
from mrfland_server import MrflandServer
import install

import mrf_proj

from mrfland_regmanager import MrflandRegManager

if __name__ == '__main__':
    #mrf_log_init()

    parse_command_line()


    rm = MrflandRegManager( {
        'http_port'       : mrf_proj.HTTP_PORT,
        'db_uri'          : install.db_uri
    })

    MrfDevHost(rm, "host", 1)

    MrfLandWebletTst(rm,
                    {
                        'tag'        : 'TST',
                        'label'      : 'Tst'

                    })
    MrfLandWebletDevs(rm,
                       {
                           'tag'  : 'DEVS',
                           'label': 'Devices'
                       })    
    MrfLandWebletHistory(rm,
                    {
                        'tag'        : 'HISTORY',
                        'label'      : 'History',
                        'start_of_records' : {
                            'month' : 12,
                            'year'  : 2017
                            },
                        'graphs': {
                            'Ambient' : { "temp" :["LOUNGE_AMBIENT","OUTSIDE_AMBIENT"]},
                            'Store'   : { "temp" :["ACC_100","ACC_60","ACC_30","ACC_RET"]},
                            'Energy'  : { "temp" :["ACC_100","ACC_60","ACC_30","OUTSIDE_AMBIENT"]},
                            'DHW1'    : { "temp" :["DHW1_100","HB1_FLOW","DHW1_HX_RET","ACC_100"],
                                          "relay" :["DHW1_HEAT","DHW1_HX_PUMP","RAD1_PUMP"]
                                         }
                        }
                    })

    ml =  MrflandServer(rm,
                        {
                            'mrfbus_host_port': mrf_proj.LAND_PORT,
                            'mrf_netid'       : mrf_proj.MRFNET ,
                            'tcp_test_port'   : mrf_proj.TEST_PORT ,
                            'console'         : mrf_proj.CONS_PORT

                        })
