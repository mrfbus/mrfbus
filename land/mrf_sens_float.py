'''
 Copyright (c) 2012-17 Gnusys Ltd
 Copyright (c) 2017-21 Steve Murphy and the owners of gnusys.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

from mrf_sens import MrfSens

from mrf_structs import *

from mrflog import mrflog

import struct

class MrfSensFloat(MrfSens):
    """ sensor recording a 32 bit floating point value : only to be used as a base class"""

    _svalue_ = 'value'
    _in_flds_ = [('date', PktTimeDate) ,
                 ('value' , float)
    ]

    _out_flds_ = [ ('send_date' , datetime.datetime.now ),
                   ('recd_date' , datetime.datetime.now),
                   ('value'  , float )
    ]

    _stype_    =  'value'

    def genout(self,indata):
        outdata = dict()
        #mrflog.info("%s input got type %s data %s"%(self.__class__.__name__, type(indata), indata))
        outdata['send_date'] = indata['date'].to_datetime()
        outdata['recd_date'] = datetime.datetime.now()

        outdata[self._svalue_] = indata['value']

        return outdata

class MrfSensFloatPercent(MrfSensFloat):
    _svalue_   = 'percent'
    _stype_    =  'percent'
    _out_flds_ = [ ('send_date' , datetime.datetime.now ),
                   ('recd_date' , datetime.datetime.now),
                   ('percent'  , float )
    ]
    def format_out_field(self,fld):
        if fld == self._svalue_:
            return "%.01f"%self.output[fld]
        else:
            return self.output[fld]


class MrfSensFloatKPower(MrfSensFloat):
    _svalue_   = 'kpower'
    _stype_    =  'kpower'
    _out_flds_ = [ ('send_date' , datetime.datetime.now ),
                   ('recd_date' , datetime.datetime.now),
                   ('kpower'  , float )
    ]
    def format_out_field(self,fld):
        if fld == self._svalue_:
            return "%.03f"%self.output[fld]
        else:
            return self.output[fld]

class MrfSensFloatPower(MrfSensFloat):
    _svalue_   = 'power'
    _stype_    =  'power'
    _out_flds_ = [ ('send_date' , datetime.datetime.now ),
                   ('recd_date' , datetime.datetime.now),
                   ('power'  , float )
    ]
    def format_out_field(self,fld):
        if fld == self._svalue_:
            return "%.0f"%self.output[fld]
        else:
            return self.output[fld]



class MrfSensFloatVolts(MrfSensFloat):
    _svalue_   = 'volts'
    _stype_    =  'volts'
    _out_flds_ = [ ('send_date' , datetime.datetime.now ),
                   ('recd_date' , datetime.datetime.now),
                   ('volts'  , float )
    ]

    def format_out_field(self,fld):
        if fld == self._svalue_:
            return "%.01f"%self.output[fld]
        else:
            return self.output[fld]

class MrfSensFloatAmps(MrfSensFloat):
    _svalue_   = 'amps'
    _stype_    =  'amps'
    _out_flds_ = [ ('send_date' , datetime.datetime.now ),
                   ('recd_date' , datetime.datetime.now),
                   ('amps'  , float )
    ]
    def format_out_field(self,fld):
        if fld == self._svalue_:
            return "%.01f"%self.output[fld]
        else:
            return self.output[fld]

class MrfSensFloatHertz(MrfSensFloat):
    _svalue_   = 'hertz'
    _stype_    =  'hertz'
    _out_flds_ = [ ('send_date' , datetime.datetime.now ),
                   ('recd_date' , datetime.datetime.now),
                   ('hertz'  , float )
    ]
    def format_out_field(self,fld):
        if fld == self._svalue_:
            return "%.02f"%self.output[fld]
        else:
            self.output[fld]

class MrfSensFloatTemp(MrfSensFloat):
    _svalue_   = 'temp'
    _stype_    =  'temp'
    _out_flds_ = [ ('send_date' , datetime.datetime.now ),
                   ('recd_date' , datetime.datetime.now),
                   ('temp'  , float )
    ]
    def format_out_field(self,fld):
        if fld == self._svalue_:
            return "%.01f"%self.output[fld]
        else:
            self.output[fld]



class MrfSensFloatPFactor(MrfSensFloat):
    _svalue_   = 'pfactor'
    _stype_    =  'pfactor'
    _out_flds_ = [ ('send_date' , datetime.datetime.now ),
                   ('recd_date' , datetime.datetime.now),
                   ('pfactor'  , float )
    ]
    def format_out_field(self,fld):
        if fld == self._svalue_:
            return "%.02f"%self.output[fld]
        else:
            self.output[fld]
