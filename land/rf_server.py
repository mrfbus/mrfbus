#! /usr/bin/env python

from tornado.options import   parse_command_line 

import mrfland

from mrfland_server import MrflandServer

from mrfland_weblet_relays import MrfLandWebletRelays
from mrfland_weblet_devs   import MrfLandWebletDevs
from mrfland_weblet_temps   import MrfLandWebletTemps

from mrfland_regmanager import MrflandRegManager
from mrfdev_host import MrfDevHost
from mrfdev_default import DevDefault
#from mrfdev_rftst import DevRftst
from mrfdev_rfmodtc import DevRfmodtc

import install
import mrf_proj

          
if __name__ == '__main__':
    parse_command_line()
    
    
    rm = MrflandRegManager(
        {
            'http_port'       : mrf_proj.HTTP_PORT,
             #'db_uri'          : install.db_uri
        })

    MrfDevHost(rm, "host", 1)
    
    DevDefault(rm, "basestation_2", 2,
              {
              })
    """
    DevRftst(rm, "rftest_20"  , 0x20,
               {
                   'relay' : ["LED1_SWITCH"]
               })
    """
    DevRfmodtc(rm, "rftst_20"  , 0x20,
               {
                   'relay' : ["LED1_SWITCH"],
                   'temp'  : ["RFTST_AMBIENT"]
               })
    
    
    DevRfmodtc(rm, "rfmodtc_21"  , 0x21,
               {
                   'relay' : ["RFTC1A_SWITCH","RFTC1B_SWITCH"],
                   'temp'  : ["RFTC1_AMBIENT"]
               })
    
    
    MrfLandWebletRelays(rm,
                        {
                            'tag':'relays',
                            'label':'Relays'
                        }
    )

    MrfLandWebletTemps(rm,
                       {
                           'tag'  : 'temps',
                           'label': 'Temperatures'
                       })
    
    MrfLandWebletDevs(rm,
                       {
                           'tag'  : 'devs',
                           'label': 'Devices'
                       })
     

    ml =  MrflandServer(rm,
                        {
                            'mrfbus_host_port': mrf_proj.LAND_PORT,
                            'mrf_netid'       : mrf_proj.MRFNET,
                            'tcp_test_port'   : mrf_proj.TEST_PORT,
                            'cons_port'       : mrf_proj.CONS_PORT
                        })
