#!/usr/bin/env python
'''  Copyright (c) 2012-17 Gnusys Ltd

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

from mrf_sens import MrfSens
from mrf_dev  import MrfDev
from mrf_sens_relay import MrfSensRelay
from mrfdev_pt1000  import MrfSensPt1000
from mrfland_weblet import MrflandWeblet, MrflandObjectTable, MrfWebletSensorVar, mrfctrl_butt_html
from mrflog import mrflog
import re
import datetime

import os
import install
import tornado
#import asyncio

class MrfLandWebletVictron(MrflandWeblet):
    _tagperiods_ =  [ {'name' : 'SMS_DIS', 'pulse' : True , 'num' : 3} ]

    _config_ = [
                 ('poll_secs' ,    5  , { 'min_val' : 1,  'max_val' :  60, 'step' : 1}),
                 ('percent_batt_msg' ,    10  , { 'min_val' : 1,  'max_val' :  50, 'step' : 1})
    ]

    def init(self):
        mrflog.info("%s init"%(self.__class__.__name__))

        # begin sanity checks

        self.targdev_name = self.cdata['targ']
        if not self.targdev_name in self.rm.devices:
            mrflog.error("%s post_init failed to find device %s in rm"%(self.__class__.__name__,self.targdev_name))
            mrflog.error("self.rm.devices was %s"%repr(self.rm.devices))
            return

        self.targdev = self.rm.devices[self.targdev_name]

        # add state var
        self.add_var('state','INIT',save=False)

        # make ref for DIS_SMS tagperiod variable

        self.disable_sms = self.var.__dict__[self.tagperiodvar['SMS_DIS']]
        self.disable_sms.label = "disable_sms"

        # create vars for all dev sensors
        for cap in list(self.targdev.caps.keys()):
            #mrflog.warn("enumerating sensors type %s"%cap)
            for ch in range(len(self.targdev.caps[cap])):
                mrflog.warn("  ch %s"%ch)
                ob = self.targdev.caps[cap][ch]
                #mrflog.warn("*****type %s label %s  outflds %s"%(type(ob), ob.label, repr(ob.out_data_flds()[0])))
                graph= ob.out_data_flds()[0] not in ['state']
                var_name = ob.label.lstrip(self.targdev_name).lstrip("_").lower()
                self.add_var(var_name,ob, field=ob.out_data_flds()[0], graph = graph)

        self.pv_pwr_list = [self.targdev_name + "_PV_DC_POWER"]
        self.pv_mppt_st_list = [self.targdev_name + "_MPPT_ST"]
        self.pv_volts_list = [self.targdev_name + "_PV_VOLTS"]
        self.pv_amps_list  = [self.targdev_name + "_PV_AMPS"]

        self.ac_graph_list = [self.targdev_name + "_AC_IN", self.targdev_name +"_AC_OUT"]
        self.pwr_ok_list   = [self.targdev_name + "_INPUT_POWER_OK"]
        self.batt_charge_list = [self.targdev_name +"_BATT_CHARGE"]
        self.batt_volts_list = [self.targdev_name +"_BATT_VOLTS"]

        mrflog.warn("*****************\nour vars are %s"%self.var.__dict__)
        #mrflog.warn("*****************\nour sens vars are %s"%self.var.sens_var_list())
    def run_init(self):
        mrflog.warn("%s run_init"%(self.__class__.__name__))
        # start timer
        self.set_timeout_secs(self.var.poll_secs.val,'meter_read', 'go')

    def _timer_callback(self,tag, act):
        mrflog.info("%s _timer_callback tag %s act %s"%(self.__class__.__name__,tag,act))

        if tag == 'meter_read':
            self.set_timeout_secs(self.var.poll_secs.val,'meter_read', 'go')
            tornado.ioloop.IOLoop.current().add_callback(self.targdev.read_meter)
    def mrfctrl_handler(self,data,wsid):
        mrflog.warn( "cmd_mrfctrl here, data was %s"%repr(data))

        return

    def subproc_callback(self, dn):
        def _sbcb(rv):
            mrflog.warn("weblet devs subproc callback for dn %s returned %d"%(dn,rv))
        return _sbcb
    def send_sms(self,msg):
        mrflog.warn("%s send_sms : %s"%(self.__class__.__name__, msg))

        #check sms disable

        if self.disable_sms.val:
            mrflog.warn("SMS is disabled by timer period, discarding message")
            return

        for it in ['sms_user','sms_passwd','sms_url','sms_num']:
            if not it in dir(install):
                mrflog.error("SMS send error - need  %s set in install.py"%it)
                return
        exe = os.path.join(os.environ['MRFBUS_HOME'],'sms.py' )
        args = ['python3']
        args.append(exe)
        args.append(msg)
        args.append('--user')
        args.append(install.sms_user)
        args.append('--passwd')
        args.append(install.sms_passwd)
        args.append('--url')
        args.append(install.sms_url)

        for num in install.sms_num:
            args.append('--num')
            args.append(num)

        mrflog.warn("SMS ARGS:" + " ".join(args))
        rv = self.rm.subprocess(args, self.subproc_callback(0))
        mrflog.warn("RV: %s"%(repr(rv)))

        #rv = self.rm.subprocess(['/usr/bin/python3', os.path.join(os.environ['MRFBUS_HOME'],'land','test_default.py' ), hex(address)] , self.subproc_callback(dn))


    def var_changed(self,name,wsid):
        #mrflog.warn("%s var_changed %s "%(self.__class__.__name__, name))

        curr_state = self.var.state.val
        #mrflog.warn("%s  self.var.input_src %s  %s  "%(self.__class__.__name__,self.var.input_src.val, repr(self.var.input_src.val)))

        if self.var.input_src.val in [ 'unknown', 'not connected']:
            next_state = 'POWER_CUT'
        else:
            next_state = 'POWER_OK'


        #determine SMS disable state

        #sms_dis = self.var.__dict__[self.tper].val

        if curr_state  ==  next_state:  # if no change in state or coming out of init return
            if curr_state == 'POWER_CUT':   # send a new SMS at each batt threshold
                if  self.var.batt_charge.raw <=  self.power_cut_msg_nxt:
                    self.power_cut_msg_nxt = self.power_cut_msg_nxt - float(self.var.percent_batt_msg.val)
                    self.send_sms("POG %s input_src %s charge_state %s batt_volts %.1f charge %.1f %%"%
                                  (next_state,self.var.input_src.val,self.var.charge_state.val,self.var.batt_volts.raw, self.var.batt_charge.raw))

            return

        if next_state == 'POWER_CUT':  # goto POWER_CUT state
            self.power_cut_batt_initial = self.var.batt_charge.raw

            self.power_cut_msg_nxt = self.power_cut_batt_initial - float(self.var.percent_batt_msg.val)
            #self.power_cut_batt_msg_next = self.power_cut_batt_initial - self.p
            mrflog.warn("%s POWER_CUT detected on var %s change  batt_volts %.1f  next msg at %.1f"%(self.__class__.__name__, name,self.var.batt_volts.raw,self.power_cut_msg_nxt ))
            self.send_sms("POG %s input_src %s charge_state %s batt_volts %.1f charge %.1f %%"%
                          (next_state,self.var.input_src.val,self.var.charge_state.val,self.var.batt_volts.raw, self.var.batt_charge.raw))


            #self.send_sms("POG %s input_src %s charge_state %s batt_volts %.1f"%
            #              (next_state,self.var.input_src.val,self.var.charge_state.val,self.var.batt_volts.raw))
            #self.send_sms("POG %s input_src %s charge_state %s batt_volts type %s"%
            #              (next_state,self.var.input_src.val,self.var.charge_state.val,type(self.var.batt_volts.val)))

        elif next_state == 'POWER_OK' and curr_state != 'INIT':
            mrflog.warn("%s POWER_OK detected on var %s change curr_state == %s"%(self.__class__.__name__, name,curr_state))
            mrflog.debug("self.var.batt_volts.value type %s "%(type(self.var.batt_volts.value)))
            self.send_sms("POG %s input_src %s charge_state %s batt_volts %.1f charge %.1f %%"%
                          (next_state,self.var.input_src.val,self.var.charge_state.val,self.var.batt_volts.raw, self.var.batt_charge.raw))


        self.var.state.set(next_state)
        return



    def pane_html(self):
        s = """ <h2>""" + self.label+ " "+ self.var.state.html + """</h2>"""


        s += """ <h3>  PV POWER """ +self.var.pv_dc_power.html+"W </h3>"
        s += self.rm.graph_inst({
            "power"  : self.pv_pwr_list,
            "mode"   : self.pv_mppt_st_list
        })


        s += """ <h3>  BATT  """ +self.var.batt_charge.html+"%    "+self.var.batt_volts.html+"V</h3>"

        s += self.rm.graph_inst({
            "percent"  : self.batt_charge_list,
            "volts"    : self.batt_volts_list
        })

        s += """ <h3>  PV ARRAY """ +self.var.pv_volts.html+"V  "+ self.var.pv_amps.html+"A</h3>"
        s += self.rm.graph_inst({
            "volts"  : self.pv_volts_list,
            "amps"   : self.pv_amps_list,

        })


        s += """ <h3> AC IN """ +self.var.ac_in.html+"W    AC OUT "+self.var.ac_out.html+"W</h3>"

        s += self.rm.graph_inst({
            "power"  : self.ac_graph_list,
            "ok"     : self.pwr_ok_list
        })


        s += """

      <hr>
        <h3>Status</h3>"""

        s += self.html_var_table(
            [self.var.state.name ] + self.var.sens_var_list()

        )

        s+= """
        <hr>
        <h3>Config</h3>"""

        s += self.html_var_ctrl_table(
            [
                self.var.poll_secs,
                self.var.percent_batt_msg

            ]
        )
        s += """
        <hr>
        <h3>Disable SMS timer</h3>

        """ + self.timer_ctrl_table(include_list=self.tagperiods['SMS_DIS'])

        return s
