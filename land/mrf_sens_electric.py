''' 
 Copyright (c) 2012-17 Gnusys Ltd
 Copyright (c) 2017-21 Steve Murphy and the owners of gnusys.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

from mrf_sens import MrfSens

from mrf_structs import *

from mrflog import mrflog

import struct

class MrfSensElectric(MrfSens):
    """ electrical sensor - using Siemens modbus device decodes METER_STATUS packets """
    _in_flds_ = [('date',  PktTimeDate) ,
                 ('resp' , PktModbusMeterState)
    ]

    _out_flds_ = [ ('send_date' , datetime.datetime.now ),
                   ('recd_date' , datetime.datetime.now),
                   ('watts'  , float ),
                   ('volts'  , float ),
                   ('amps'   , float ),
                   ('hertz'  , float ),
    ]

    _stype_    =  'modbus_electric'

    def obval2str(self,oblen,obval):
        s = ""
        for i in range(oblen):
            try:
                s+=  chr(obval[i])
            except:
                mrflog.warn("ERROR have i=%d   oblen %d obval %s"%(i,oblen,repr(obval)))
                    #import pdb
                    #pdb.set_trace()
        #s[oblen] = '\0'
        return s
        
    def append_obj(self,outdata,obnum,oblen,obval):
        s = self.obval2str(oblen,obval)
        if obnum == 0:
            outdata['vendor'] = s
        elif obnum == 1:
            outdata['product'] = s
        elif obnum == 2:
            outdata['revision'] = s
        else:
            mrflog.error("invalid obnum %d",obnum)
        return outdata
        
                   
    def genout(self,indata):
        resp = indata['resp']
        mrflog.warn(" indata =  %s "%(repr(indata)))
        mrflog.warn("type resp %s , len %d "%(type(resp),len(resp)))
        s = ""
        for c in resp:
            s += "%02x "%c

        mrflog.warn(s)
        #import pdb
        #pdb.set_trace()
        rbytes = bytes(resp)
        hdr = PktModbusReadRespHdr()
        hdr.load(rbytes)
        mrflog.warn(repr(hdr))

        rbytes = rbytes[len(hdr):]

        mrflog.warn("remaining %d : %s"%(len(rbytes),repr(rbytes)))

        fval = struct.unpack('>f',rbytes[0:4])[0]
        mrflog.warn("fval : %f",fval)

        
        #for i in range(
        outdata = dict()
        #mrflog.info("%s input got type %s data %s"%(self.__class__.__name__, type(indata), indata))
        outdata['send_date'] = indata['date'].to_datetime()
        outdata['recd_date'] = datetime.datetime.now()

        outdata['volts'] = fval
        outdata['watts'] = 0.0
        outdata['amps'] = 0.0
        



        return outdata
                    
        #resplen = indata['resplen']
        
        
        #if resplen < 20:
        #    mrflog.error("invalid resplen , got %02x",resplen)
        #    return None

        for i in range(len(intro)):
            if resp[i] != intro[i]:
                mrflog.error("expected 0x%02x in resp[%d] , got %02x",intro[i],i,resp[i])

                str1 = ""
                for i in range(len(resp)):
                    str1 += "%02x "%resp[i]

                mrflog.error("resp was :"+str1)
                return None
                

        rem = resp[len(intro):]

        for ob in range(3):
            obnum = rem[0]
            oblen = rem[1]
            obval = rem[2:2+oblen]
            mrflog.warn("obnum %d oblen %d obval %s"%(int(obnum),int(oblen),str(obval)))
            del (rem[0:2+oblen])
            outdata = self.append_obj(outdata, obnum,oblen,obval)

        return outdata
