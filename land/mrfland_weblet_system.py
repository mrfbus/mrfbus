#!/usr/bin/env python
'''  Copyright (c) 2012-17 Gnusys Ltd

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

from mrf_sens import MrfSens
from mrf_dev  import MrfDev
from mrf_sens_relay import MrfSensRelay
from mrfdev_pt1000  import MrfSensPt1000
from mrfland_weblet import MrflandWeblet, MrflandObjectTable, MrfWebletSensorVar, mrfctrl_butt_html
from mrflog import mrflog
import re
import datetime

import os
import install
import tornado
#import asyncio

class MrfLandWebletSystem(MrflandWeblet):

    _config_ = [
                 ('poll_secs' ,    5  , { 'min_val' : 5,  'max_val' :  60, 'step' : 1})
    ]

    def init(self):
        mrflog.info("%s init"%(self.__class__.__name__))

        # begin sanity checks

        self.targdev_name = self.cdata['targ']
        if not self.targdev_name in self.rm.devices:
            mrflog.error("%s post_init failed to find device %s in rm"%(self.__class__.__name__,self.targdev_name))
            mrflog.error("self.rm.devices was %s"%repr(self.rm.devices))
            return

        self.targdev = self.rm.devices[self.targdev_name]


        # create vars for all dev sensors
        for cap in list(self.targdev.caps.keys()):
            #mrflog.warn("enumerating sensors type %s"%cap)
            for ch in range(len(self.targdev.caps[cap])):
                mrflog.warn("  ch %s"%ch)
                ob = self.targdev.caps[cap][ch]
                #mrflog.warn("*****type %s label %s  outflds %s"%(type(ob), ob.label, repr(ob.out_data_flds()[0])))
                graph= ob.out_data_flds()[0] not in ['started']
                var_name = ob.label.lstrip(self.targdev_name).lstrip("_").lower()
                self.add_var(var_name,ob, field=ob.out_data_flds()[0], graph = graph)

        self.perc_list = [self.targdev_name + "_PCPU",self.targdev_name + "_PMEM"]
        self.mem_list = [self.targdev_name + "_RSS",self.targdev_name + "_SZ",self.targdev_name + "_VSZ"]
        mrflog.warn("*****************\nour vars are %s"%self.var.__dict__)
        #mrflog.warn("*****************\nour sens vars are %s"%self.var.sens_var_list())
    def run_init(self):
        mrflog.warn("%s run_init"%(self.__class__.__name__))
        # start timer
        self.set_timeout_secs(self.var.poll_secs.val,'meter_read', 'go')

    def _timer_callback(self,tag, act):
        #mrflog.warn("%s _timer_callback tag %s act %s"%(self.__class__.__name__,tag,act))

        if tag == 'meter_read':
            self.targdev.start_read()
            self.set_timeout_secs(self.var.poll_secs.val,'meter_read', 'go')
    def mrfctrl_handler(self,data,wsid):
        mrflog.warn( "cmd_mrfctrl here, data was %s"%repr(data))

        return





    def pane_html(self):
        s = """ <h2>""" + self.label+ " Uptime " +  self.var.uptime.html + """</h2>"""

        s += """ <h3>  UTILISATION  CPU """ +self.var.pcpu.html+"%  MEMORY "+ self.var.pmem.html + "% </h3>"

        #mrflog.warn("%s pane_html perc_list = %s"%(self.__class__.__name__,repr(self.perc_list)))
        s += self.rm.graph_inst({
            "percent"  : self.perc_list
        })

        s += """ <h3>  MEMORY USAGE  </h3>"""

        mrflog.warn("%s pane_html mem_list = %s"%(self.__class__.__name__,repr(self.mem_list)))
        s += self.rm.graph_inst({
            "kb"  : self.mem_list
        })



        
        s+= """
      <hr>

        <h3>Status</h3>"""

        s += self.html_var_table(
            self.var.sens_var_list()

        )

        s+= """
        <hr>
        <h3>Config</h3>"""

        s += self.html_var_ctrl_table(
            [
                self.var.poll_secs

            ]
        )

        s+= """
        <hr>
        """
        s += self.jsvar_cb_html('consoledebug')

        return s
