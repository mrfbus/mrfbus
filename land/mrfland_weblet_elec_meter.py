
#!/usr/bin/env python
'''  Copyright (c) 2012-17 Gnusys Ltd

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
from mrfdev_pt1000 import *
from mrf_sens import MrfSens
from mrf_sens_modbus_id import MrfSensModbusId
from mrf_sens_float  import *
from mrf_dev  import MrfDev
from mrfland_weblet import MrflandWeblet, MrflandObjectTable
from mrfdev_rfmodbus import POWER_FIELDS
from mrflog import mrflog
import re
import os

class MrfLandWebletElecMeter(MrflandWeblet):

    _config_ = [
        ('test_freq_secs' ,  5  , { 'min_val' : 1,  'max_val' :  20, 'step' : 1})
    ]


    _tagperiods_  = [{'name':'EN','pulse' :True , 'num' : 2}]
    
    def init(self):
        mrflog.info("%s init"%(self.__class__.__name__))
        # do subscriptions here
        ## looking for all MrfSensModbusId

        if MrfSensModbusId not in self.rm.senstypes:
            mrflog.error("%s post_init failed to find sensor type MrfSensModbusId in rm"%self.__class__.__name__)
            return
        self.sl = self.rm.senstypes[MrfSensModbusId]

        mrflog.warn("num MrfSensModbusId found was %d"%len(self.sl))
        self.slabs = []
        self.sens = OrderedDict()
        for s in self.sl:
            self.slabs.append(s.label)
            self.sens[s.label] = s
        mrflog.warn("MrfSensModbusId : %s"%repr(self.slabs))

        for s in list(self.sens.keys()):
            self.sens[s].subscribe(self.sens_callback)
        

        self.test_active = {}

        for sn in self.slabs:
            self.test_active[sn] = False

        # find period sensor



        sn = self.tag + "_EN" + "_PERIOD"
        self.periodsens = self.rm.sens_search(sn)
        self.add_var('active',self.periodsens, field='active')

        # add vars for electric sensor and subscribe

        # will graph power and volts to start

        self.update_idx  = 0  #cycle through graph items on timer
        sensors = self.cdata['sensors']

        #self.sensors = {} # keep lookup
        for sn in sensors:
            slabs = sensors[sn]
            for slab in slabs:
                sens = self.rm.sens_search(slab)
                #self.sensors[sn] = sens
                if sn == 'kpower':
                    vn = None
                    for n in POWER_FIELDS:
                        if slab.lower().startswith(n):
                            vn = n
                            break
                    if vn == None:
                        mrflog.error("Couldn't deduce power variable name in label "+slab)
                        sys.exit(-1)

                    mrflog.warn("Adding sensor var %s using sensor label %s  type %s"%(sn,sens.label,sens.__class__.__name__))
                    self.add_var(vn,sens,field=sn, graph= True)

                else:
                    mrflog.warn("Adding sensor var %s using sensor label %s  type %s"%(sn,sens.label,sens.__class__.__name__))
                    self.add_var(sn,sens,field=sn, graph= True)

    def sens_callback(self, label, data ):
        mrflog.debug("%s : sens_callback  %s  data %s"%(self.__class__.__name__,label,repr(data)))
        self.rm.webupdate(self.mktag(self.tag, label), data)


    def timer_callback(self,label,act):
        mrflog.warn("timer_callback %s act %s"%(str(label),str(act)))
        if self.var.active.val : # if active restart
            self.start_test()
        
    def pane_html(self):
        """ display Schneider modbus meter readings """

        s = """
        <h2>%s"""%self.label+"    "+self.var.kwatts.html+ """ kW </h2>
        <hr>
         <h3>
         <table class="table">
          <thead>
            <tr>
             <th>kW</th>
             <th>kVAR</th>
             <th>kVA</th>
             <th>PF</th>
            </tr>
          </thead>
          <tbody>
           <tr><td> """+self.var.kwatts.html+""" </td>
               <td> """+self.var.kvar.html+""" </td>
               <td> """+self.var.kva.html+""" </td>
               <td> """+self.var.pfactor.html+""" </td>
            </tr>
         </tbody>
        </table>
      <hr>
         <table class="table">
          <thead>
            <tr>
             <th>volts</th>
             <th>amps</th>
             <th>hertz</th>
            </tr>
          </thead>
          <tbody>
           <tr>
               <td> """+self.var.volts.html+""" </td>
               <td> """+self.var.amps.html+""" </td>
               <td> """+self.var.hertz.html+""" </td>
            </tr>
         </tbody>
        </table>

        </h3>
        """
        s +=  """
       <hr> """

        s += self.rm.graph_inst({
            "kpower"  :  self.cdata['sensors']['kpower']
        })

        s += self.rm.graph_inst({
            "pfactor"  :  self.cdata['sensors']['pfactor']
        })
        
        s += self.rm.graph_inst({
            "volts"  :  self.cdata['sensors']['volts'],
            "amps"   :  self.cdata['sensors']['amps']
        })
        

        if len(self.sl):
            s += MrflandObjectTable(self.tag,self.tag,self.sl[0]._output,self.slabs,  postcontrols = [("start_test","_mrf_ctrl_butt")],iclasses={'start_test' : 'glyphicon-check'})
        return s


    def mrfctrl_handler(self,data, wsid=None):

        mrflog.warn( "cmd_mrfctrl here, data was %s"%repr(data))

        """
        if 'fld' not in data or data['fld'] not in A9MEM2150_FUNCS:
            mrflog.warn( "not start_test - doing nothing")
            return
        """
        if data['fld'] == 'start_test':
            param = PktUint8()
            param.value = 1  # address
            self.sl[0].devupdate('READ_DEVICE_ID',param)
        else:
            mrflog.error("unknown command "+ data['fld'])
        return

    def run_init(self):
        mrflog.warn("runt_init reading device id")

        #self.update_idx += 1

        param = PktUint8()
        param.value = 1  # address
        self.sl[0].devupdate('READ_DEVICE_ID',param)
        #self.rm.server._run_updates()
        
