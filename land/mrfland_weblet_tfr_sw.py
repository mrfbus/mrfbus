#!/usr/bin/env python
'''  Copyright (c) 2012-17 Gnusys Ltd

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


MrfLandWebletTfrSw

Control transfer switch

  Idea is to select victron/PV supply if battery is above a certain level

  Ideally change over when load is low , to preserve life of cheap ATS used in circuit

'''

from mrf_sens import MrfSens
from mrf_dev  import MrfDev
from mrf_sens_relay import MrfSensRelay
from mrf_sens_float  import *
from mrfland_weblet import MrflandWeblet, MrflandObjectTable, MrfWebletSensorVar, mrfctrl_butt_html
from mrflog import mrflog
import re
import datetime

class MrfLandWebletTfrSw(MrflandWeblet):

    _config_ = [
        ('recovery' ,    10 , { 'min_val' : 5,  'max_val' :  50, 'step' : 1}),
        ('min_batt' ,    50 , { 'min_val' : 25,  'max_val' :  90, 'step' : 1}),
        ('window'   ,    5  , { 'min_val' : 0,  'max_val' :  10, 'step' : 1}),
        ('max_power',    500 , { 'min_val' : 100,  'max_val' :  3000, 'step' : 100}),
        ('power_trip',    4000 , { 'min_val' : 2000,  'max_val' :  6000, 'step' : 100}),
        ('power_recovery', 1500 , { 'min_val' : 500,  'max_val' :  2500, 'step' : 100}),
        ('trip_timeout_mins', 10 , { 'min_val' : 1,  'max_val' :  60, 'step' : 1})
   ]

    _tagperiods_  = [{'name':'DIS_TFR', 'num' : 2}]

    def init(self):
        mrflog.info("%s init"%(self.__class__.__name__))

        mrflog.warn("%s init , cdata = \n %s"%(self.__class__.__name__,repr(self.cdata)))
        # begin sanity checks

        ## expect MrfSensFloatPower types

        if MrfSensFloatPower not in self.rm.senstypes:
            mrflog.error("%s post_init failed to find sensor type MrfSensFloatPower in rm"%self.__class__.__name__)
            return

        ## expect MrfSensFloatPercent types

        if MrfSensFloatPercent not in self.rm.senstypes:
            mrflog.error("%s post_init failed to find sensor type MrfSensFloatPercent in rm"%self.__class__.__name__)
            return

        ## expect MrfSensRelay types

        if MrfSensRelay not in self.rm.senstypes:
            mrflog.error("%s post_init failed to find sensor type MrfSensRelay in rm"%self.__class__.__name__)
            return


        ## expect config data fields as follows
        if 'load' not in self.cdata:
            mrflog.error("%s , no load in data"%self.__class__.__name__)
            return

        if 'grid' not in self.cdata:
            mrflog.error("%s , no grid in data"%self.__class__.__name__)
            return

        if 'battery' not in self.cdata:
            mrflog.error("%s , battery in data"%self.__class__.__name__)
            return


        if 'switch' not in self.cdata:
            mrflog.error("%s , no switch in data"%self.__class__.__name__)
            return



        # end sanity checks

        # find period sensor

       # make ref for DIS_TFR tagperiod variable  - used to prevent transfer to backup supply

        self.disable_tfr = self.var.__dict__[self.tagperiodvar['DIS_TFR']]
        self.disable_tfr.label = "disable_tfr"


        # find battery level sensor

        self.battery = self.rm.sens_search(self.cdata['battery'])
        self.add_var('battery',self.battery, field='percent', graph=True)


        # find load sensor

        self.load = self.rm.sens_search(self.cdata['load'])
        self.add_var('load',self.load, field='power', graph=True)

        # find grid sensor

        self.grid = self.rm.sens_search(self.cdata['grid'])
        self.add_var('grid',self.grid, field='kpower', graph=True)


        # find switch relay

        self.switch = self.rm.sens_search(self.cdata['switch'])
        self.add_var('switch',self.switch, field='relay', graph=True)


        ## declare state var

        self.add_var('state','UP', save=False)  # values UP DOWN , i.e. hysterisis ref select

        """
        ## normal vars to keep state of algo

        self.store_state  = 'UP'

        self.ambient_state  = 'UP'
        """


    def run_init(self):
        mrflog.warn("%s run_init"%(self.__class__.__name__))
        # start timer




    def mrfctrl_handler(self,data,wsid):
        mrflog.warn( "cmd_mrfctrl here, data was %s"%repr(data))

        return

    def timer_callback(self, label, act):
        mrflog.warn("%s : timer_callback label %s act %s "%(self.__class__.__name__,label,act))
        if self.var.state.val == 'TRIP':
            self.var.state.val == 'TRIP_TO'
            mrflog.warn("%s : moving state from TRIP to TRIP_TO after timeout expired "%(self.__class__.__name__))

            self.var_changed("STATE_FORCE", "na")
            
    def set_timeout(self,seconds):
        # start timer
        mrflog.warn("%s : set_timeout seconds  %d  "%(self.__class__.__name__,seconds))
        now = datetime.now()
        td  = timedelta(seconds = seconds)
        tod = now + td
        self.set_timer( tod.time() , 'state' , 'TO')

    def sw_next(self):
        """ simple logic to determine of tfr switch should be enabled """
        sw_curr = self.var.switch.val

        if self.var.state.val == 'TRIP':  # keep sw off until timeout expires
            return 0 , 'TRIP'


        if self.disable_tfr.val:  # if disabled by timer , ensure tfr switch is off ( i.e. ensure connected to grid )
            return 0 , 'UP'


       # handle power limit exceeded

        if (self.var.load.raw > self.var.power_trip.val):
            mrflog.warn("power TRIP reached on tfr_switch load %s %s"%(repr(self.var.load.raw),repr(self.var.power_trip.val)))
            self.set_timeout(60*self.var.trip_timeout_mins.val)  # refuse to turn switch back on until timeout has expired
            mrflog.warn("set timeout to %d secs - returning"%(60*self.var.trip_timeout_mins.val))
            return 0, "TRIP"


        if self.var.state.val == 'TRIP_TO':
            if self.var.load.raw < (self.var.power_trip.val - power_recovery): # can remove trip condition
                mrflog.warn("removing TRIP_TO condition - going to UP")
                return 0, "UP"
            else:
                return 0, "TRIP_TO"



        # handled missed relay set commands - if we are DOWN then relay should be on , if UP should be off

        if self.var.state.val == 'DOWN' and not sw_curr:
            mrflog.warn("relay in wrong state - should be on")
            return 1, self.var.state.val

        if self.var.state.val == 'UP' and sw_curr:
           mrflog.warn("relay in wrong state - should be off")
           return 0, self.var.state.val




        """
        mrflog.warn("type self.var.battery.raw %s val %.1f"%(type(self.var.battery.raw),self.var.battery.raw))
        mrflog.warn("repr %s"%repr(self.var.battery))
        mrflog.warn("repr %s"%repr(self.var.battery.sens))
        mrflog.warn("type self.var.min_batt.val %s"%type(self.var.min_batt.val))
        #import pdb
        #pdb.set_trace()
        mrflog.warn("state.val %s"%(self.var.state.val))
        mrflog.warn("sw_curr %d"%(sw_curr))
        mrflog.warn("high thresh1 %.1f"%( self.var.min_batt.val + self.var.recovery.val + self.var.window.val))
        """
        # first check of we turn off or on regardless of power , or are waiting inside opportunity window

        if self.var.state.val == 'UP' and self.var.battery.raw > ( self.var.min_batt.val + self.var.recovery.val+self.var.window.val):  # turn on regardless of power being pulled
            return 1 , 'DOWN'

        if self.var.state.val == 'DOWN' and self.var.battery.raw < ( self.var.min_batt.val - self.var.window.val):  # turn off regardless of power being pulled
            return 0 , 'UP',


        # if load is over threshold we return now keeping switch and state unchanged
        #mrflog.warn("load.raw %.1f  max_power %.1f"%( self.var.load.raw,self.var.max_power.val))

        if (self.var.load.raw > self.var.max_power.val) or (self.var.grid.raw > self.var.max_power.val/1000.0):
            return sw_curr , self.var.state.val


        # now check if we should switch on

        if self.var.state.val == 'UP' and self.var.battery.raw > ( self.var.min_batt.val + self.var.recovery.val):  # turn on
            return 1 , 'DOWN'

        if self.var.state.val == 'DOWN' and self.var.battery.raw < ( self.var.min_batt.val):  # turn off
            return 0 , 'UP',

        # should never get here
        #mrflog.error("%s - error processing sw_next..please check (we're refusing to change anything )"%self.__class__.__name__)
        return sw_curr , self.var.state.val




    def var_changed(self,name,wsid):
        """ ignore state var changes here """
        if name == 'state':
            mrflog.warn("ignoring var %s change "%(name))
            return
   
        #mrflog.warn("%s var_changed %s "%(self.__class__.__name__, name))
        
        sw_curr = self.var.switch.val

        sw_next ,state_next  = self.sw_next()

        if self.var.state.val != state_next: # update state
            mrflog.warn("state var changed from %s to %s , sw_curr %s sw_next %s"%(self.var.state.val,state_next,repr(sw_curr),repr(sw_next)))
            self.var.state.set(state_next)    # only make mrfvar for debug aid

        
        # set relay if not already correct
        if sw_next != sw_curr:
            mrflog.warn("setting transfer switch to %s after var %s changed "%(repr(sw_next),name))
            self.switch.set(sw_next)
        #else:
        #    mrflog.warn("leaving pump %s"%repr(pump_next))




    def pane_html(self):
        #mrflog.error("self.var.battery.label %s"%self.var.battery.label)
        s =  """
        <h2>"""+self.label+"</h2>"

        s += """ <h3>  BATTERY """ +self.var.battery.html+"%   SWITCH "+ self.var.switch.html+"</h3>"

        s += self.rm.graph_inst({
            "percent" : [self.var.battery.sens.label],  # FFS!!! what is it with this..why not
            "relay": [self.var.switch.sens.label]
        })

        s += """ <h3>  LOAD """ +self.var.load.html+"W  </h3>"

        s += self.rm.graph_inst({
            "power" : [self.var.load.sens.label],
            "relay": [self.var.switch.sens.label]
        })


        s += """ <h3>  GRID """ +self.var.grid.html+"kW  </h3>"

        s += self.rm.graph_inst({
            "kpower" : [self.var.grid.sens.label],
            "relay": [self.var.switch.sens.label]
        })


        s += """
        <hr>
        <h3>Status</h3>"""

        s += self.html_var_table(
             [self.var.state.name ] + self.var.sens_var_list()
        )

        s += """
        <hr>
        <h3>Config</h3>"""

        s += self.html_var_ctrl_table(
            [
                self.var.min_batt.name,
                self.var.max_power.name,
                self.var.recovery.name,
                self.var.window.name,
                self.var.power_trip,
                self.var.power_recovery,
                self.var.trip_timeout_mins
            ]
        )

        s += """
        <hr>
        <h3>Disable switch timer</h3>

        """ + self.timer_ctrl_table()

        return s
