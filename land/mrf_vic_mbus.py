
from pymodbus.constants import Defaults
from pymodbus.constants import Endian
from pymodbus.client.tcp import ModbusTcpClient as ModbusClient
from pymodbus.payload import BinaryPayloadDecoder

from pymodbus.register_read_message import ReadInputRegistersResponse
from mrflog import mrflog


from collections import OrderedDict

Defaults.Timeout = 25
Defaults.Retries = 5

import pdb

class MbValue(object):
    __rtypes__ = {
        'uint16' : { 'count' : 1, 'decode' : 'decode_16bit_uint'} ,
        'int16'  : { 'count' : 1, 'decode' : 'decode_16bit_int'} ,
        'uint32' : { 'count' : 2, 'decode' : 'decode_32bit_uint'} ,
        'int32'  : { 'count' : 2, 'decode' : 'decode_32bit_int'}
    }
    def __init__(self,name, unit, regnum, rtype, scale):
        self.name   = name
        self.regnum = regnum
        self.unit   = unit
        if not rtype in self.__rtypes__:
            print("ERROR invalid rtype %s for MbValue %s"%(rtype,name))
            return
        self.rtype  = self.__rtypes__[rtype]
        self.scale  = scale

    def value(self,client):
        #pdb.set_trace()
        result = client.read_input_registers(self.regnum, self.rtype['count'])
        decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big)
        #pdb.set_trace()
        dmeth   = getattr(decoder,self.rtype['decode'])
        v       = dmeth()
        if self.scale != 1:
            v = v/self.scale
        return v

    def val_dict(self,client):
        """ return dict repr of self """
        rv = dict()
        rv['name'] = self.name
        rv['unit'] = self.unit
        rv['value'] = self.value(client)
        return rv
        
    def repr(self,client):
        v = self.value(client)

        if type(self.unit) == dict:
            return "%-20s: %s"%(self.name,self.unit[v])
        if type(v) == float:
            return "%-20s: %.2f %s"%(self.name,v,self.unit)
        else:
            return "%-20s: %d %s"%(self.name,v,self.unit)

class VicModbusReader(object):

    def add_value(self,name,unit,rnum,rt,scale):
        self.mbvalues[name]   = MbValue(name,unit  , rnum ,rt,scale)

    def get_value(self,name):
        return  self.mbvalues[name].repr(self.client)

    def get_val_dict(self,name):
        return  self.mbvalues[name].val_dict(self.client)
        #rv = await self.mbvalues[name].val_dict(self.client)
        #return rv
        #return  self.mbvalues[name].val_dict(self.client)
    
    def __init__(self):
        self.mbvalues = OrderedDict()

        self.add_value('INPUT_SRC',
                  {0: 'unknown', 1 : 'grid', 2 : 'generator', 3: 'shore', 240: 'not connected'},
                  826,'int16',1)

        self.add_value('AC_OUT','W', 817,'uint16',1)
        #self.add_value('AC_consumption_L2','W', 818,'uint16',1)
        #self.add_value('AC_consumption_L3','W', 819,'uint16',1)

        self.add_value('AC_IN','W', 820,'uint16',1)
        #self.add_value('Grid_L2','W', 821,'uint16',1)
        #self.add_value('Grid_L3','W', 822,'uint16',1)

        self.add_value('BATT_VOLTS','V'  , 840,'uint16',10.0)
        self.add_value('BATT_AMPS','A'  , 841,'int16',10.0)
        self.add_value('BATT_POWER','W'  , 842,'int16',1)
        self.add_value('BATT_CHARGE','%', 843,'uint16',1)
        self.add_value('CHARGE_STATE',
                  {0: 'idle', 1 : 'charging', 2 : 'discharging'},
                  844,'int16',1)
        self.add_value('PV_DC_POWER','W'  , 850,'uint16',1)
        self.add_value('PV_DC_AMPS','A'  , 851,'int16',10)

        self.add_value('CHARGER_POWER','W'  , 855,'uint16',1)
        self.add_value('DC_SYS_POWER','W'  , 860,'int16',1)

        self.client = ModbusClient('nanopi', port='502')

    def get_values(self):
        """ return dict of values"""
        rv = OrderedDict()
        for v in self.mbvalues.keys():
            print(self.get_value(v))

    def get_all_dict(self):
        rv = OrderedDict()
        for v in self.mbvalues.keys():
            rv[v] = self.get_val_dict(v)
        # want to pass a simple grid power OK value here
        d = {'name':'INPUT_POWER_OK', 'unit' :'OK'}
        #mrflog.warn("rv : "+repr(rv))
        #mrflog.warn("rv['INPUT_SRC'] %d"%rv['INPUT_SRC']['value'])
        
        if rv['INPUT_SRC']['value'] in [ 0, 240] :
            d["value"] = 0
        else:
            d["value"] = 1
        rv['INPUT_POWER_OK'] = d
        return rv
    
    def print_values(self):
        for v in self.mbvalues.keys():
            print(self.get_value(v))


if __name__ == '__main__':
    mbc = VicModbusReader()

    mbc.print_values()



