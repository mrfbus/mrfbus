#!/usr/bin/env python
'''  Copyright (c) 2012-17 Gnusys Ltd

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

from mrf_sens import MrfSens
from mrf_dev  import MrfDev
import datetime
import ctypes
from mrf_structs import *
from core_tests import mrf_cmd_app_test
from math import sqrt
from mrflog import mrflog
from collections import deque
from mrf_sens_modbus_id import MrfSensModbusId
from mrf_sens_relay import MrfSensRelay
#from mrf_sens_electric import MrfSensElectric
from mrfdev_rfmodtc import PktRfmodtcState, MrfSensTempX100
from mrf_sens_float import *
from mrf_sens_str import MrfSensStrState
from mrf_sens_int import MrfSensIntOK, MrfSensIntMode
#from mrfdev_pt1000 import PktRelayState, MrfSensPtRelay

import tornado.gen as gen
from mrf_vic_ambus import *

class PktVictronModbusState(MrfStruct):
    _fields_ = [

        ("ac_in_kw"        , c_float),
        ("ac_out_kw"       , c_float),
        ("battery_kw"      , c_float),
        ("pv_dc_kw"        , c_float),
        ("charger_kw"      , c_float),
        ("dc_sys_kw"       , c_float),
        ("battery_volts"   , c_float),
        ("battery_amps"    , c_float),
        ("pv_dc_amps"      , c_float),
        ("battery_charge"  , c_float),
        ("volts_in"        , c_float),
        ("volts_out"       , c_float),
        ("batt_temp"       , c_float),
        ("ve_state"        , c_uint8*20),
        ("charger_state"   , c_uint8*16)
    ]

mrf_cmd_read_meter = 128


VicModbusAppCmds = {
    mrf_cmd_read_meter : {
        'name'  : "READ_METER",
        'param' : PktUint8,
        'resp'  : PktVictronModbusState
    },

}



# essentially map from sensor chan to func for power variables
POWER_FIELDS = ['kwatts','kvar', 'kva']

class VDevVicModbus(MrfDev):

    _capspec = {
        'power'   : MrfSensFloatPower,
        'volts'    : MrfSensFloatVolts,
        'amps'     : MrfSensFloatAmps,
        'percent'  : MrfSensFloatPercent,
        'state'    : MrfSensStrState,
        'temp'     : MrfSensFloatTemp,
        'ok'       : MrfSensIntOK,
        'mode'     : MrfSensIntMode
    }
    _cmdset = VicModbusAppCmds

    def init(self):
        self.filt_ind = 0
        self.mbc = VicModbusReader()
        #self.read_meter()

    #@gen.coroutine

    async def connect_meter(self):
        if not mbc.client.connected:
            await mbc.client.connect()
    async def read_meter(self):
        dres =  await self.mbc.get_all_dict()

        for c in self.caps.keys():
            #mrflog.warn("%s %s capspec %s type %s "%(self.__class__.__name__,self.label, c, type(self.caps[c])))
            for ob in self.caps[c]:
                cmps = ob.label.split("_")
                if cmps[0] != self.label:
                    mrflog.error("NO not us with %s !!!"%(cmps[0]))
                    continue
                #mrflog.warn("yes got us with %s "%(cmps[0]))
                sn = "_".join(cmps[1:])
                #mrflog.warn("   %s type %s sn %s"%(ob.label, type(ob),sn))
                if sn  not in dres:
                    mrflog.error("NO!! cannot see %s in dres!!"%(sn))
                    continue
                dob = dres[sn]
                #mrflog.warn("   dob is %s c = %s"%(repr(dob),c))
                now = datetime.datetime.now()
                td =  PktTimeDate()
                td.set(now)
                if c == "kpower" and dob['unit'] == 'W':

                    val = dob['value'] / 1000.0

                elif c == "state":
                    val = dob['unit'][dob['value']]
                elif c in  ["ok","mode"]:
                    val = int(dob['value'])

                else:
                     val = float(dob['value'])

                inp = { 'date' : td,
                        'value' : val
                }
                ob.input(inp)

        mrflog.debug("vic dict is \n%s"%repr(dres))

    def app_packet(self, hdr, param , resp):
        """ should not happen for VDEV """
        mrflog.info("%s app_packet type %s"%(self.__class__.__name__, type(resp)))

        mrflog.error("%s app_packet, hdr %s param %s resp %s"%(self.__class__.__name__,
                                                              repr(hdr), repr(param), repr(resp)))
        return
