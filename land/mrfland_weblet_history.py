#!/usr/bin/env python
'''  Copyright (c) 2012-17 Gnusys Ltd

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
from mrfland_weblet import MrflandWeblet, mrfctrl_butt_html, mrfctrl_select_html
from mrflog import mrflog
from mrfland import to_json
import datetime
from dateutil.relativedelta import  relativedelta
import calendar
from mrfland_regmanager import MrflandRegManager  # FYI self/ __MrflandWeblet__.rm is this type

class MrfLandWebletHistory(MrflandWeblet):

    def init(self):
        mrflog.info("%s init"%(self.__class__.__name__))
        self.graphid = "graph1"
        self.days = 1
        self.graph_type = 'Ambient'

        if 'start_of_records' in self.cdata:
            self.sor = self.cdata['start_of_records']
        else:
            self.sor =  None

        if not 'graphs' in self.cdata:
            mrflog.error("%s init : no graphs dictionary in cdata!!"%(self.__class__.__name__))
            return
        else:
            self.graphs = self.cdata['graphs']
            self.graph_list = list(self.graphs.keys())
        self.graph_type = self.graph_list[0]
        
        self.days_opts = [1,7,28]

    def day_select_values(self):
        """ create select values for day select """
        self.sel_mon_num = []
        self.sel_year_num = []
        self.day_sel_days = []
        self.day_sel_vals = []
        for do in self.days_opts:
            self.day_sel_vals.append("%d"%do)
            self.day_sel_days.append(do)
            self.sel_mon_num.append(0)
            self.sel_year_num.append(0)
            
        self.month_number_lookup = {}
        if not self.sor:
            return

        
        self.html_now_date = datetime.datetime.combine(datetime.datetime.now().date(),datetime.time())
        self.docdate = self.html_now_date - datetime.timedelta(days=0)
        start_month = self.sor['month']
        start_year  = self.sor['year']

        cur_month  = self.html_now_date.month
        cur_year   = self.html_now_date.year

        month = cur_month
        year = cur_year
        mrflog.warn("start_month %d start_year %d"%(start_month,start_year))
        if  month == start_month and year == start_year:
            return
        
        while True: 
            monrange = calendar.monthrange(year,month)
            monname =  calendar.month_abbr[month]
            if not monname in self.month_number_lookup:
                self.month_number_lookup[monname] = month
            sval = monname + " %d"%year
            self.day_sel_vals.append(sval)
            self.day_sel_days.append(monrange[1])
            self.sel_mon_num.append(month)
            self.sel_year_num.append(year)
            mrflog.debug("added sel "+sval)

            if month == start_month and year == start_year:
                break
            month -= 1
            if month == 0:
                month = 12
                year = year - 1
                mrflog.debug("rolled year back to %d"%year)
            

    def mrfctrl_handler(self,data,wsid):
        mrflog.debug("mrfctrl_handler here, data was %s"%repr(data))

        if data['tab'] != self.graphid:
            return




        if data['row'] == 'days':
            idx = self.day_sel_vals.index(data['value'])
            self.days = self.day_sel_days[idx]
            if idx < len(self.days_opts): # days up to now
                self.docdate = self.html_now_date - datetime.timedelta(days=self.days_opts[idx]-1)
            else:
                self.docdate = datetime.datetime(self.sel_year_num[idx],self.sel_mon_num[idx],1)

                
        elif data['row'] == 'type':
            self.graph_type = data['value']
        if not self.graph_type in self.graph_list:
            mrflog.error("graph_type %s not in list %s"%(self.graph_type, repr(self.graph_list)))
            return
        sensors = self.graphs[self.graph_type]

        #get midnight today to match docdate for today
        #docdate = datetime.datetime.combine(datetime.datetime.now().date(),datetime.time())

        self.rm.db_days_graph(sensors=sensors,docdate=self.docdate,wsid=wsid, wtag=self.graphtag(self.graphid),days=self.days)

    def pane_html(self):

        s = """<hr>
        <div>
        """

        
        self.day_select_values()
        
        s += mrfctrl_select_html(self.tag,self.graphid, 'days', self.day_sel_vals,'Days')
        s += mrfctrl_select_html(self.tag,self.graphid, 'type', self.graph_list,'Graph')

        s += """
      </div>
"""

        s += '<div id="'+self.graphid+'"></div>'

        if hasattr(self.rm,'db_sensors'):
            sens = self.rm.db_sensors
            mrflog.warn("pane_html got sensors %s"%repr(sens))
        else:
            sens = None

        if False:  # sens:    FIXME - what is this , generates javascript browser error - NOT VALID!!
            s += """
<script type="text/javascript">
"""+to_json(sens)+"""
</script>

"""
        return s
