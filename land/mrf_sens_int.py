'''
 Copyright (c) 2012-17 Gnusys Ltd
 Copyright (c) 2017-21 Steve Murphy and the owners of gnusys.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

from mrf_sens import MrfSens

from mrf_structs import *

from mrflog import mrflog

import struct

class MrfSensInt(MrfSens):
    """ sensor recording an int value """

    _svalue_ = 'value'
    _in_flds_ = [('date', PktTimeDate) ,
                 ('value' , int)
    ]

    _out_flds_ = [ ('send_date' , datetime.datetime.now ),
                   ('recd_date' , datetime.datetime.now),
                   ('value'  , int )
    ]

    _stype_    =  'intsensor'

    def genout(self,indata):
        outdata = dict()
        #mrflog.info("%s input got type %s data %s"%(self.__class__.__name__, type(indata), indata))
        outdata['send_date'] = indata['date'].to_datetime()
        outdata['recd_date'] = datetime.datetime.now()

        outdata[self._svalue_] = indata['value']
        return outdata


class MrfSensIntKB(MrfSensInt):
    """ int sensor for kilobytes """
    _svalue_   = 'kb'
    _stype_    =  'kb'
    _out_flds_ = [ ('send_date' , datetime.datetime.now ),
                   ('recd_date' , datetime.datetime.now),
                   ('kb'  , int )
                  ]
    
class MrfSensIntOK(MrfSensInt):
    """ int sensor signifying OK status 0,1 """
    _svalue_   = 'ok'
    _stype_    =  'ok'
    _out_flds_ = [ ('send_date' , datetime.datetime.now ),
                   ('recd_date' , datetime.datetime.now),
                   ('ok'  , int )
                  ]

class MrfSensIntMode(MrfSensInt):
    """ int sensor reporting mode/enum as int  """
    _svalue_   = 'mode'
    _stype_    =  'mode'
    _out_flds_ = [ ('send_date' , datetime.datetime.now ),
                   ('recd_date' , datetime.datetime.now),
                   ('mode'  , int )
                  ]
