#!/usr/bin/env python
'''  Copyright (c) 2012-17 Gnusys Ltd

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

from mrf_sens import MrfSens
from mrf_dev  import MrfDev
import datetime
import ctypes
from mrf_structs import *
from core_tests import mrf_cmd_app_test
from math import sqrt
from mrflog import mrflog
from collections import deque
from mrf_sens_modbus_id import MrfSensModbusId
from mrf_sens_relay import MrfSensRelay
#from mrf_sens_electric import MrfSensElectric
from mrfdev_rfmodtc import PktRfmodtcState, MrfSensTempX100
from mrf_sens_float import *
from mrf_sens_str import MrfSensStrState, MrfSensStr
from mrf_sens_int import MrfSensIntOK, MrfSensIntMode, MrfSensIntKB
from mrf_sens_time import MrfSensTime
#from mrfdev_pt1000 import PktRelayState, MrfSensPtRelay
import os
import dateparser





# essentially map from sensor chan to func for power variables
POWER_FIELDS = ['kwatts','kvar', 'kva']

class VDevSysMon(MrfDev):

    _capspec = {
        'percent' : MrfSensFloatPercent,
        'kb'      : MrfSensIntKB,
        'started' : MrfSensTime,
        'uptime'  : MrfSensStr
    }
    _cmdset = {}


    def callback(self,dobj):
        #mrflog.warn("got callback dobj %s"%repr(dobj))
        retval = dobj['retval']
        output = dobj['output']
        lines = output.split('\n')
        hdr = lines[0]
        vals = lines[1]
        #mrflog.warn("hdr:"+hdr)
        #mrflog.warn("vals:"+vals)

        hs = hdr.split()
        vs = vals.split()
        
        d = {}
        quit = False
        for h in hs:
            if h[0] == "%":
                nh = "P"+h[1:]
            else:
                nh = h
            if h == "STARTED":
                v = " ".join(vs)
                quit = True
            else:
                v = vs[0]
                vs = vs[1:]
            d[nh] = v
            if quit:
                break

        # create uptime string

        started = dateparser.parse(d['STARTED'])
        uptime = datetime.datetime.now() - started

        cmps = str(uptime).split('.')  # get rid of microseconds if found
        if len(cmps) > 1:            
            upstr = cmps[0]
        else:
            str(uptime)
        d['UPTIME'] = upstr
        
        #mrflog.warn("d : %s"%repr(d))
        for c in self.caps.keys():
            #mrflog.warn("%s %s capspec %s type %s "%(self.__class__.__name__,self.label, c, type(self.caps[c])))
            l = self.caps[c]
            for it in l:
                cmps = it.label.split("_")
                tok = "_".join(cmps[1:])
                
                if tok in d:
                    now = datetime.datetime.now()
                    td =  PktTimeDate()
                    td.set(now)
                    
                    inp = { 'date' : td,
                            'value' : d[tok]
                           }
                    #mrflog.warn("it._input['value'] = "+repr(it._input['value']))
                    #mrflog.warn("setting %s input(%s)"%(it.label,repr(inp)))
                    it.input(inp)
                else:
                    mrflog.error("cap %s does not map to data dict %s"%repr(d))
                
                #mrflog.warn("tok %s it %s  type %s  label %s"%(tok,repr(it),type(it),it.label))
        
        
    def start_read(self,dobj={}):
        args = ['/bin/ps','-p', "%s"%os.getpid(), '-o', 'pcpu,pmem,rss,sz,vsz,lstart']
        dobj['msg'] = 'hello'
        self.rm.server.subprocess2(args,self.callback,dobj)

    def app_packet(self, hdr, param , resp):
        """ should not happen for VDEV """
        mrflog.info("%s app_packet type %s"%(self.__class__.__name__, type(resp)))

        mrflog.error("%s app_packet, hdr %s param %s resp %s"%(self.__class__.__name__,
                                                              repr(hdr), repr(param), repr(resp)))
        return
