/******************************************************************************
*
* Copyright (c) 2012-16 Gnusys Ltd
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
******************************************************************************/

#include "mrf_sys.h"
#include <mrf_debug.h>


#include  <msp430.h>
#include <legacymsp430.h>
#include "cc430f5137.h"
#include "mrf_pinmacros.h"
#include "mrf_arch.h"
#include "mrf_relays.h"
#include "mrf_route.h"
#include "mrf_modbus.h"
#include "mrf_timer.h"
#include  "mrf_sys_structs.h"

#include "rtc_arch.h"
#include "stdio.h"

void mrf_rf_idle(I_F i_f);

//#define _BUT1_PORT P1
//#define _BUT1_BIT  1

//#define _CS_PORT P1
//#define _CS_BIT  7



uint8 _app_mode;


typedef enum {
  SYNC_TIME,
  SHOW_TIME} APP_STATE;

typedef enum {
  ADC_TEMP,
  ADC_VCC} ADC_CHAN;




static APP_STATE _app_state;
static MRF_ROUTE _base_route;
static ADC_CHAN _adc_chan;

static uint16 _adc_res;  // vcc div 2 and temp

static uint32_t tgrad100, tycross100;
static uint16_t _sec_count;

static uint32_t _last_reading[MAX_RTDS];
static MRF_PKT_RFMODTC_STATE _state_pkt;

static uint32_t _ftaps[10];

ModbusReadDeviceID read_dev_id; 
ModbusReadHoldingRegs read_hold_regs; 

// codes

//#define APP_CMD_MODBUS_READ_DEV_ID 32
//#define APP_CMD_UART_TIMER_CALLBACK 50

//const MRF_IF_TYPE (*uart_if) = & mrf_uart_cc_if;
 
int mrf_app_init(){


  mrf_modbus_init(BAUD_19200,PAR_ODD);
  
  read_dev_id       =  ModbusReadDeviceID();
  read_hold_regs    =  ModbusReadHoldingRegs();
  
  for (int  i=0 ; i<MAX_RTDS ; i++){
    _last_reading[i] = 0;
  }
  _app_state = SYNC_TIME;
  mrf_nexthop(&_base_route, MRFID, 0);  // get relay/basestation route
  
  //INPUTPIN(BUT1);
  //P1REN &= ~BITNAME(BUT1);
  //P1IES |=  BITNAME(BUT1);  // hi to lo 
  //P1IE  |=  BITNAME(BUT1);

  P3DIR = 0xFF;
  P3OUT = 0x0;

  P2DIR = 0xFC;
  P2OUT = 0x0;

  _app_mode = 0;
  
  _sec_count = 0;
  init_relays();
  mrf_timer_init();
  //PINLOW(CS);
  //OUTPUTPIN(CS);

  tgrad100 =  (100*((uint32_t)*((uint16_t *)0x1A1C) - *((uint16_t *)0x1A1A)))/55;  // cal points for 85 and 30C
  tycross100 =   (100*(uint32_t)(*((uint16_t *)0x1A1A))) - (30*tgrad100);


  _adc_chan = ADC_TEMP;
  // set up ADC

  REFCTL0 = REFMSTR+REFVSEL_0+REFON;  //+REFTCOFF;

  //ADC12CTL0 = ADC12SHT02 + ADC12ON;         // Sampling time, ADC12 on
  ADC12CTL0 =   ADC12SHT0_8 + ADC12ON;         // Sampling time, ADC12 on
  ADC12CTL1 = ADC12SHP;                     // Use sampling timer
  ADC12MCTL0 = ADC12SREF_1 + ADC12INCH_10;  // ADC input ch A10 => temp sense
  ADC12CTL0 |= ADC12ENC;

  
  rtc_second_signal_enable();
  return 0;
}

int mrf_sleep_deep(){
  mrf_rf_idle(RF0);
  return 0;
}

char _message[20];


int _dbg_ping_res(){
  return -21;
}

MRF_CMD_RES mrf_task_usr_resp(MRF_CMD_CODE cmd,uint8 bnum, const MRF_IF *ifp){
   MRF_PKT_HDR *hdr = (MRF_PKT_HDR *)_mrf_buff_ptr(bnum);
   MRF_PKT_RESP *resp = (MRF_PKT_RESP *)(((uint8 *)hdr)+ sizeof(MRF_PKT_HDR));
   if (_app_state == SYNC_TIME ){
     if(resp->type == mrf_cmd_get_time){
       TIMEDATE *td = (TIMEDATE *)((uint8*)resp + sizeof(MRF_PKT_RESP));
       rtc_set(td);
       _app_state = SHOW_TIME;
     }
   }
   else if (resp->type == mrf_cmd_ping) {     
     MRF_PKT_PING_RES *pres = (MRF_PKT_PING_RES *)((uint8_t *)hdr + sizeof(MRF_PKT_HDR)+ sizeof(MRF_PKT_RESP));

     if (_app_mode == 1) {
       sprintf(_message,"R S%02X Q%02X",pres->from_rssi,pres->from_lqi);
       _dbg_ping_res();

     }
   }
  _mrf_buff_free(bnum);
  return MRF_CMD_RES_OK;
}

MRF_CMD_RES mrf_task_usr_struct(MRF_CMD_CODE cmd,uint8 bnum, const MRF_IF *ifp){
  _mrf_buff_free(bnum);
  return MRF_CMD_RES_OK;
}

MRF_CMD_RES mrf_app_task_test(MRF_CMD_CODE cmd,uint8 bnum, const MRF_IF *ifp){
  mrf_debug("%s","mrf_app_task_test entry\n");
  uint8 *rbuff = mrf_response_buffer(bnum);
  mrf_rtc_get((TIMEDATE *)rbuff);
  mrf_send_response(bnum,sizeof(TIMEDATE));
  mrf_debug("%s","mrf_app_task_test exit\n");
  return MRF_CMD_RES_OK;
}


MRF_CMD_RES mrf_app_set_relay(MRF_CMD_CODE cmd,uint8 bnum, const MRF_IF *ifp){
  MRF_PKT_RELAY_STATE *rs;
  mrf_debug("mrf_app_task_relay entry\n");
  rs = (MRF_PKT_RELAY_STATE *)((uint8 *)_mrf_buff_ptr(bnum) + sizeof(MRF_PKT_HDR));
  set_relay_state(rs->chan,rs->val);
  rs->val = get_relay_state(rs->chan);
  mrf_data_response( bnum,(uint8 *)rs,sizeof(MRF_PKT_RELAY_STATE));  
  return MRF_CMD_RES_OK;
}

MRF_CMD_RES mrf_app_led_on(MRF_CMD_CODE cmd,uint8 bnum, const MRF_IF *ifp){
  MRF_PKT_RELAY_STATE rs;
  mrf_debug("mrf_app_task_relay entry\n");
  set_relay_state(0,1);
  rs.chan = 0;
  rs.val = get_relay_state(rs.chan);
  mrf_data_response( bnum,(uint8 *)&rs,sizeof(MRF_PKT_RELAY_STATE));  
  return MRF_CMD_RES_OK;
}
MRF_CMD_RES mrf_app_led_off(MRF_CMD_CODE cmd,uint8 bnum, const MRF_IF *ifp){
  MRF_PKT_RELAY_STATE rs;
  mrf_debug("mrf_app_task_relay entry\n");
  set_relay_state(0,0);
  rs.chan = 0;
  rs.val = get_relay_state(rs.chan);
  mrf_data_response( bnum,(uint8 *)&rs,sizeof(MRF_PKT_RELAY_STATE));  
  return MRF_CMD_RES_OK;
}


MRF_CMD_RES mrf_app_get_relay(MRF_CMD_CODE cmd,uint8 bnum, const MRF_IF *ifp){
  MRF_PKT_RELAY_STATE *rs;
  mrf_debug("mrf_app_task_relay entry\n");
  rs = (MRF_PKT_RELAY_STATE *)((uint8 *)_mrf_buff_ptr(bnum) + sizeof(MRF_PKT_HDR));
  rs->val = get_relay_state(rs->chan);
  mrf_data_response( bnum,(uint8 *)rs,sizeof(MRF_PKT_RELAY_STATE));  
  return MRF_CMD_RES_OK;
}



void button_pressed(){

  _app_mode = ( _app_mode + 1 ) % 2;
  

}

static unsigned int P1IVREG;

interrupt(PORT1_VECTOR) PORT1_ISR (void) 
{
  // switch(__even_in_range(P1IV, 16))
 
  P1IVREG = P1IV;
  switch(P1IVREG)
  {
    case  0: break;
    case  2:break;                         // P1.0 IFG
    case  4: 
      P1IE &= ~BIT1;                             // Debounce by disabling buttons
      button_pressed();

      
      //   __bic_SR_register_on_exit(LPM3_bits); // Exit active   
      break;                         // P1.1 IFG
    case  6: break;                         // P1.2 IFG
    case  8: break;                         // P1.3 IFG
    case 10: break;                         // P1.4 IFG
    case 12: break;                         // P1.5 IFG
    case 14: break;                         // P1.6 IFG
    case 16: break;
  }
}


int build_state(MRF_PKT_RFMODTC_STATE *state){

  //uint16 rd = ads1148_data();

  mrf_rtc_get(&((*state).td));
  uint8 ch;
  for (ch = 0 ; ch < MAX_RTDS ; ch++){
    (*state).tempX100[ch] = _last_reading[ch];
  }
  (*state).relay_cmd   = 0;
  (*state).relay_state = relay_state();
  return 0;
}



MRF_CMD_RES mrf_app_read_state(MRF_CMD_CODE cmd,uint8 bnum, const MRF_IF *ifp){
  mrf_debug("mrf_app_read_state entry bnum %d\n",bnum);
  MRF_PKT_RFMODTC_STATE *state = (MRF_PKT_RFMODTC_STATE *)mrf_response_buffer(bnum);
  build_state(state);
  mrf_send_response(bnum,sizeof(MRF_PKT_RFMODTC_STATE));
  mrf_debug("mrf_app_read_state exit\n");
  return MRF_CMD_RES_OK;

}


// async response
int modbus_dev_id_ready(PROC_TAG ptag){
  //uint8_t resp_len = read_dev_id.resp_len;
  //uint8_t *rptr =  (uint8_t *)(_mrf_buff_ptr(ptag.bnum) + sizeof(MRF_PKT_HDR)+ sizeof(MRF_PKT_RESP));
  mrf_send_response( ptag.bnum,ptag.param1);
  return 0;
}


MRF_CMD_RES mrf_app_read_devid(MRF_CMD_CODE cmd,uint8 bnum, const MRF_IF *ifp){
  uint8_t  *respbuff;
  MRF_PKT_UINT8 *param;
  PROC_TAG ptag;
  mrf_debug("mrf_app_read_devid entry bnum %d\n",bnum);
  param = (MRF_PKT_UINT8 *)((uint8 *)_mrf_buff_ptr(bnum) + sizeof(MRF_PKT_HDR));
  respbuff = ((uint8 *)_mrf_buff_ptr(bnum) + sizeof(MRF_PKT_HDR)+ sizeof(MRF_PKT_RESP));
  //uint8_t *rptr =  (uint8_t *)(_mrf_buff_ptr(bnum) + sizeof(MRF_PKT_HDR)+ sizeof(MRF_PKT_RESP));
  ptag.cmd = _MRF_APP_QCMD_BASE + APP_CMD_MODBUS_READ_DEV_ID;
  ptag.bnum = bnum;
  ptag.param1 = 0; //param->value;
  ptag.param2 = 0;

  read_dev_id.issue_cmd(param->value,ptag, respbuff); // should have stat on rv error FIXME

  return MRF_CMD_RES_OK;
}


// async response - looks like this is same for all modbus
int modbus_hold_regs_ready(PROC_TAG ptag){
  //uint8_t resp_len = read_hold_regs.resp_len;
  //uint8_t *rptr =  (uint8_t *)(_mrf_buff_ptr(ptag.bnum) + sizeof(MRF_PKT_HDR)+ sizeof(MRF_PKT_RESP));
  mrf_send_response( ptag.bnum,ptag.param1+2 ); // bit messy - add the 3 on for the start address + moving to odd alignment
  return 0;
}


MRF_CMD_RES mrf_app_read_holdregs(MRF_CMD_CODE cmd,uint8 bnum, const MRF_IF *ifp){
  uint8_t  *respbuff;
  MRF_PKT_MODBUS_READ *param;
  PROC_TAG ptag;
  mrf_debug("mrf_app_read_devid entry bnum %d\n",bnum);
  param = (MRF_PKT_MODBUS_READ *)((uint8 *)_mrf_buff_ptr(bnum) + sizeof(MRF_PKT_HDR));
  respbuff = ((uint8 *)_mrf_buff_ptr(bnum) + sizeof(MRF_PKT_HDR)+ sizeof(MRF_PKT_RESP));
  //uint8_t *rptr =  (uint8_t *)(_mrf_buff_ptr(bnum) + sizeof(MRF_PKT_HDR)+ sizeof(MRF_PKT_RESP));
  ptag.cmd = _MRF_APP_QCMD_BASE + APP_CMD_MODBUS_READ_HOLD_REGS;
  ptag.bnum = bnum;
  ptag.param1 = 0; //param->value;
  ptag.param2 = 0;

  // save start addr as first uint16 of response buffer

  *((uint16 *)respbuff) = param->start_addr;
  // align respbuff to odd , so that add,func,bytecount leaves register values aligned in response
  //respbuff[3] = 0;
  
  read_hold_regs.issue_cmd(param->addr,ptag, respbuff+2, param->start_addr, param->num_regs); // should have stat on rv error FIXME

  return MRF_CMD_RES_OK;
}

static uint8_t _modbus_meter_resp[16]; 

//static uint8_t _modbus_meter_item_idx;
static uint8_t _modbus_meter_addr;
static MRF_PKT_METER_STATE *_modbus_meter_statep;

// Schneider modbus meter reg addresses

#define A9MEM2150_REG_AMPS    2999
#define A9MEM2150_REG_VOLTS   3027
#define A9MEM2150_REG_KWATTS  3053
#define A9MEM2150_REG_KVAR    3067
#define A9MEM2150_REG_KVA     3075
#define A9MEM2150_REG_PFACTOR 3083
#define A9MEM2150_REG_HERTZ   3109


// async response - iterate through multiple calls of read_hold_regs.issue_cmd to build packet
int modbus_read_meter_ready(PROC_TAG ptag){
  //uint8_t resp_len = read_hold_regs.resp_len;
  //uint8_t *rptr =  (uint8_t *)(_mrf_buff_ptr(ptag.bnum) + sizeof(MRF_PKT_HDR)+ sizeof(MRF_PKT_RESP));
  uint16_t next_reg_addr;
  uint8_t *dbs = _modbus_meter_resp + 3;
  uint8_t *dbd ;
  // switch on item idx in ptag.param2
  switch (ptag.param2) {
  case 0  : dbd = (uint8_t *)&_modbus_meter_statep->amps   ; next_reg_addr = A9MEM2150_REG_VOLTS; break;
  case 1  : dbd = (uint8_t *)&_modbus_meter_statep->volts  ; next_reg_addr = A9MEM2150_REG_KWATTS; break;
  case 2  : dbd = (uint8_t *)&_modbus_meter_statep->kwatts ; next_reg_addr = A9MEM2150_REG_KVAR; break;
  case 3  : dbd = (uint8_t *)&_modbus_meter_statep->kvar   ; next_reg_addr = A9MEM2150_REG_KVA ; break;
  case 4  : dbd = (uint8_t *)&_modbus_meter_statep->kva    ; next_reg_addr = A9MEM2150_REG_PFACTOR; break;
  case 5  : dbd = (uint8_t *)&_modbus_meter_statep->pfactor    ; next_reg_addr = A9MEM2150_REG_HERTZ; break;
  default : dbd = (uint8_t *)&_modbus_meter_statep->hertz  ; break;
  }
    // copy 4 bytes of result
  for (int i = 0 ; i < 4 ; i++) {
    *(dbd+i) = *(dbs + 3 - i); // try BE to LE swapping here of 32 bit FP value
  }

  if (ptag.param2 < 6) {  // more regs to read
    ptag.cmd = _MRF_APP_QCMD_BASE + APP_CMD_MODBUS_READ_METER_RDY; //possibly wrong to return ptag with _MRF_APP_QCMD_BASE subtracted -please review

    ptag.param2++; // use this for idx
    read_hold_regs.issue_cmd(_modbus_meter_addr,ptag, _modbus_meter_resp,next_reg_addr , 2); // should have stat on rv error FIXME
    
  }
  else {
    // all readings complete - fill in rest of packet and send now

    mrf_rtc_get(&((*_modbus_meter_statep).td));
    _modbus_meter_statep->tempX100    = _last_reading[0];
    _modbus_meter_statep->relay_state = relay_state();

    if(0xff == ptag.bnum) // sending status structure
      mrf_send_structure(0,  _MRF_APP_CMD_BASE + mrf_app_cmd_read_meter,  (uint8 *)_modbus_meter_statep, sizeof(MRF_PKT_METER_STATE));

    else // response to read_meter command
      mrf_send_response( ptag.bnum,sizeof(MRF_PKT_METER_STATE) ); 
  }
  return 0;
}

int setup_meter_read(uint8 bnum, uint8_t mbus_addr, MRF_PKT_METER_STATE *pktp){
  // looking to build METER_STATUS_PKT , either as response or usr struct
  // will leave all 4/x/n floating point values in memory pointed to by buff
  // _modbus_meter_item_idx = 0;
  PROC_TAG ptag;
  _modbus_meter_statep   = pktp;
  _modbus_meter_addr     = mbus_addr;
  ptag.cmd = _MRF_APP_QCMD_BASE + APP_CMD_MODBUS_READ_METER_RDY;
  ptag.bnum = bnum;
  ptag.param1 = 0; // this is used by modbus handler to report response length;
  ptag.param2 = 0; // use this for idx

  // first modbus reg - amps
  read_hold_regs.issue_cmd(_modbus_meter_addr,ptag, _modbus_meter_resp, A9MEM2150_REG_AMPS, 2); // should have stat on rv error FIXME
  
  return 0;
}

MRF_CMD_RES mrf_app_read_meter(MRF_CMD_CODE cmd,uint8 bnum, const MRF_IF *ifp){
  uint8_t  *respbuff;
  MRF_PKT_UINT8 *param;
  mrf_debug("mrf_app_read_devid entry bnum %d\n",bnum);
  param = (MRF_PKT_UINT8 *)((uint8 *)_mrf_buff_ptr(bnum) + sizeof(MRF_PKT_HDR));
  respbuff = ((uint8 *)_mrf_buff_ptr(bnum) + sizeof(MRF_PKT_HDR)+ sizeof(MRF_PKT_RESP));
  //uint8_t *rptr =  (uint8_t *)(_mrf_buff_ptr(bnum) + sizeof(MRF_PKT_HDR)+ sizeof(MRF_PKT_RESP));
  setup_meter_read(bnum,param->value,(MRF_PKT_METER_STATE *)respbuff);
  

  return MRF_CMD_RES_OK;
}


// MODBUS RS485 mrf_modbus_cc.c needs async timer callback for turnaround timing


int _Dbg_ping(){
  return -15;
}
MRF_PKT_METER_STATE pkt_meter_state;  
int sec_task(){
  //sprintf(_message,"sec %d",_sec_count);
  //MRF_ROUTE route;
  P1IE |= BIT1;                             // hmpff -renable button ( crude debouncing )

  if (_app_state == SYNC_TIME){ // relying on usr_resp to change state when response received
    mrf_send_command(_base_route.relay,  mrf_cmd_get_time,  NULL, 0);
  }
  else {  // SHOW TIME!
    _sec_count++;

    // run temp measure each second 
    ADC12IE = 0x01;                           // Enable interrupt
    ADC12CTL0 |= ADC12SC;                   // Start sampling/conversion

      
  }

  if ((_sec_count % STATUS_NUM_SECS) == 0){  //send  meter state packet every STATUS_NUM_SECS secs
    setup_meter_read(0xff, 1 , &pkt_meter_state);
  }
  return 0;
}

int adc_res_ready(){
  uint32_t temp,avg;
  //uint16 adcres  = _adc_res;
  int i;
  temp = (100*(uint32_t)_adc_res)-tycross100;
  temp = 100* temp / tgrad100;

  temp = temp - 270;  // single point calibration done wrt. modtc thermocouple device.. ho ho
  temp = temp - 130;  // correction for another board??  reading about 1.3C over
  temp = temp / 10;
  temp = temp * 10;  // zero lsb


  if ((_sec_count % STATUS_NUM_SECS) == 0){  // send avg every 10 secs
    avg = 0;
    for ( i = 0 ; i < STATUS_NUM_SECS ; i++)
      avg = avg + _ftaps[i];

    avg = avg / STATUS_NUM_SECS;
    _last_reading[0] = avg; // only one channel here
    //build_state(&_state_pkt);
    //mrf_send_structure(0,  _MRF_APP_CMD_BASE + mrf_app_cmd_read_state,  (uint8 *)&_state_pkt, sizeof(MRF_PKT_RFMODTC_STATE));

  }
  _ftaps[_sec_count % 10 ] = temp;

  return 0;
}



// this is run by mrf_foreground - so a foreground task
// needs taking into system with jump tables - this would allow modules to manage their own handlers and simplify app code - FIXME 
MRF_CMD_RES app_queue_process(PROC_TAG ptag) {
  uint8_t cmd = ptag.cmd; // % (_MRF_APP_CMD_BASE);
  if (cmd == APP_SIG_SECOND){
    sec_task();
    return MRF_CMD_RES_OK;
  }
  else if (cmd == APP_SIG_ADC_RES){
    adc_res_ready();
    return MRF_CMD_RES_OK;
  }
  else if (cmd == APP_CMD_MODBUS_READ_DEV_ID){
    modbus_dev_id_ready(ptag);
    return MRF_CMD_RES_OK;
  }
  else if (cmd == APP_CMD_MODBUS_READ_HOLD_REGS){
    modbus_hold_regs_ready(ptag);
    return MRF_CMD_RES_OK;
  }
  else if (cmd == APP_CMD_MODBUS_TURN_AROUND){
    mrf_modbus_turn_around();
    return MRF_CMD_RES_OK;
  }
  else if (cmd == APP_CMD_MODBUS_READ_METER_RDY){
    modbus_read_meter_ready(ptag);
    return MRF_CMD_RES_OK;
  }
  else if (cmd == APP_CMD_MODBUS_START_TX){
    mrf_modbus_start_tx();
    return MRF_CMD_RES_OK;
  }
  
  return  MRF_CMD_RES_WARN;  // FIXME better to have stat for app cmd decode err
}


int _adc_res_rdy(){
  ADC12IE = 0x0;                           // disable interrupt
  _adc_res = ADC12MEM0;

  mrf_app_signal(APP_SIG_ADC_RES,0,0);

  return 0;
}


interrupt(ADC12_VECTOR) ADC12ISR(void)
{

  switch(ADC12IV)
  {
  case  0: break;                           // Vector  0:  No interrupt
  case  2: break;                           // Vector  2:  ADC overflow
  case  4: break;                           // Vector  4:  ADC timing overflow
  case  6:                                  // Vector  6:  ADC12IFG0

    ADC12IE = 0x0;                           // disable interrupt

    _adc_res_rdy();
    if (mrf_wake_on_exit())
      __bic_SR_register_on_exit(LPM3_bits);

    break;
  case  8: break;                           // Vector  8:  ADC12IFG1
  case 10: break;                           // Vector 10:  ADC12IFG2
  case 12: break;                           // Vector 12:  ADC12IFG3
  case 14: break;                           // Vector 14:  ADC12IFG4
  case 16: break;                           // Vector 16:  ADC12IFG5
  case 18: break;                           // Vector 18:  ADC12IFG6
  case 20: break;                           // Vector 20:  ADC12IFG7
  case 22: break;                           // Vector 22:  ADC12IFG8
  case 24: break;                           // Vector 24:  ADC12IFG9
  case 26: break;                           // Vector 26:  ADC12IFG10
  case 28: break;                           // Vector 28:  ADC12IFG11
  case 30: break;                           // Vector 30:  ADC12IFG12
  case 32: break;                           // Vector 32:  ADC12IFG13
  case 34: break;                           // Vector 34:  ADC12IFG14
  default: break;
  }
}
